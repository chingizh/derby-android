
package com.derby.models.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("content")
    @Expose
    private Content content;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("fromUserName")
    @Expose
    private String fromUserName;
    @SerializedName("fromUserPhotoFilePath")
    @Expose
    private String fromUserPhotoFilePath;
    @SerializedName("reading")
    @Expose
    private Integer reading;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getFromUserPhotoFilePath() {
        return fromUserPhotoFilePath;
    }

    public void setFromUserPhotoFilePath(String fromUserPhotoFilePath) {
        this.fromUserPhotoFilePath = fromUserPhotoFilePath;
    }

    public Integer getReading() {
        return reading;
    }

    public void setReading(Integer reading) {
        this.reading = reading;
    }

}

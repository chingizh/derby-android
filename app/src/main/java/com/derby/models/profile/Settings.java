
package com.derby.models.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Settings {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("newMatches")
    @Expose
    private Boolean newMatches;
    @SerializedName("newMessages")
    @Expose
    private Boolean newMessages;
    @SerializedName("supportMessages")
    @Expose
    private Boolean supportMessages;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getNewMatches() {
        return newMatches;
    }

    public void setNewMatches(Boolean newMatches) {
        this.newMatches = newMatches;
    }

    public Boolean getNewMessages() {
        return newMessages;
    }

    public void setNewMessages(Boolean newMessages) {
        this.newMessages = newMessages;
    }

    public Boolean getSupportMessages() {
        return supportMessages;
    }

    public void setSupportMessages(Boolean supportMessages) {
        this.supportMessages = supportMessages;
    }

}

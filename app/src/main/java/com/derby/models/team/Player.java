
package com.derby.models.team;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Player {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("teamCapitan")
    @Expose
    private Boolean teamCapitan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getTeamCapitan() {
        return teamCapitan;
    }

    public void setTeamCapitan(Boolean teamCapitan) {
        this.teamCapitan = teamCapitan;
    }

}


package com.derby.models.team;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("photoFileId")
    @Expose
    private Integer photoFileId;
    @SerializedName("logoUrl")
    @Expose
    private String logoUrl;
    @SerializedName("players")
    @Expose
    private List<Player> players = null;
    @SerializedName("playerCount")
    @Expose
    private Integer playerCount;
    @SerializedName("capitanName")
    @Expose
    private String capitanName;
    @SerializedName("capitanId")
    @Expose
    private Integer capitanId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhotoFileId() {
        return photoFileId;
    }

    public void setPhotoFileId(Integer photoFileId) {
        this.photoFileId = photoFileId;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Integer getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(Integer playerCount) {
        this.playerCount = playerCount;
    }

    public String getCapitanName() {
        return capitanName;
    }

    public void setCapitanName(String capitanName) {
        this.capitanName = capitanName;
    }

    public Integer getCapitanId() {
        return capitanId;
    }

    public void setCapitanId(Integer capitanId) {
        this.capitanId = capitanId;
    }

}

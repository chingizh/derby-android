
package com.derby.models.games;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateTeam {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("photoFileId")
    @Expose
    private Integer photoFileId;
    @SerializedName("logoUrl")
    @Expose
    private String logoUrl;
    @SerializedName("players")
    @Expose
    private Object players;
    @SerializedName("playerCount")
    @Expose
    private Integer playerCount;
    @SerializedName("capitanName")
    @Expose
    private Object capitanName;
    @SerializedName("capitanId")
    @Expose
    private Integer capitanId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhotoFileId() {
        return photoFileId;
    }

    public void setPhotoFileId(Integer photoFileId) {
        this.photoFileId = photoFileId;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Object getPlayers() {
        return players;
    }

    public void setPlayers(Object players) {
        this.players = players;
    }

    public Integer getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(Integer playerCount) {
        this.playerCount = playerCount;
    }

    public Object getCapitanName() {
        return capitanName;
    }

    public void setCapitanName(Object capitanName) {
        this.capitanName = capitanName;
    }

    public Integer getCapitanId() {
        return capitanId;
    }

    public void setCapitanId(Integer capitanId) {
        this.capitanId = capitanId;
    }

}

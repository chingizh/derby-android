
package com.derby.models.games;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("createTeam")
    @Expose
    private CreateTeam createTeam;
    @SerializedName("otherTeam")
    @Expose
    private Object otherTeam;
    @SerializedName("startDay")
    @Expose
    private String startDay;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("createUserId")
    @Expose
    private Integer createUserId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CreateTeam getCreateTeam() {
        return createTeam;
    }

    public void setCreateTeam(CreateTeam createTeam) {
        this.createTeam = createTeam;
    }

    public Object getOtherTeam() {
        return otherTeam;
    }

    public void setOtherTeam(Object otherTeam) {
        this.otherTeam = otherTeam;
    }

    public String getStartDay() {
        return startDay;
    }

    public void setStartDay(String startDay) {
        this.startDay = startDay;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

}

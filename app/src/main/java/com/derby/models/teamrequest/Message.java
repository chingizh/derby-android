
package com.derby.models.teamrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("az")
    @Expose
    private String az;
    @SerializedName("en")
    @Expose
    private String en;
    @SerializedName("ru")
    @Expose
    private String ru;

    public String getAz() {
        return az;
    }

    public void setAz(String az) {
        this.az = az;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getRu() {
        return ru;
    }

    public void setRu(String ru) {
        this.ru = ru;
    }

}

package com.derby.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.derby.R;
import com.derby.models.teamrequest.Datum;

import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class AcceptDeclineTeamViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Datum> modelList;

    private OnItemClickListener mItemClickListener;


    public AcceptDeclineTeamViewAdapter(Context context, List<Datum> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(List<Datum> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_accept_decline_team_list, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final Datum model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;

            if (!model.getTeam().getLogoUrl().isEmpty())
                Glide.with(genericViewHolder.userImage.getContext()).load(model.getTeam().getLogoUrl()).into(genericViewHolder.userImage);
            genericViewHolder.whoAccepted.setText(String.format(genericViewHolder.whoAccepted.getContext().getString(R.string.s_wants_to_add_you), model.getTeam().getName()));
        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private Datum getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, Datum model);

        void onAcceptClick(View view, int position, Datum model);

        void onDeclineClick(View view, int position, Datum model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_image)
        CircleImageView userImage;
        @BindView(R.id.team_image)
        ImageView teamImage;
        @BindView(R.id.who_accepted)
        TextView whoAccepted;
        @BindView(R.id.accept)
        Button accept;
        @BindView(R.id.decline)
        Button decline;

        public ViewHolder(final View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(view -> mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition())));

            accept.setOnClickListener(view -> mItemClickListener.onAcceptClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition())));

            decline.setOnClickListener(view -> mItemClickListener.onDeclineClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition())));

        }
    }

}


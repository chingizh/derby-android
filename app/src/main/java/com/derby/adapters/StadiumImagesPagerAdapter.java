package com.derby.adapters;

/**
 * Created by chingizh on 2019-07-11 long live Chingiz!
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.derby.R;

import java.util.ArrayList;

import androidx.viewpager.widget.PagerAdapter;

public class StadiumImagesPagerAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<String> images;
    private LayoutInflater layoutInflater;


    public StadiumImagesPagerAdapter(Context context, ArrayList<String> images) {
        this.context = context;
        this.images = images;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem( ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.stadium_slider_item, container, false);

        ImageView imageView = itemView.findViewById(R.id.imageView);
        Glide.with(context).load(images.get(position)).into(imageView);
//        imageView.setImageResource(images[position]);

        container.addView(itemView);

        //listening to image click
        imageView.setOnClickListener(v -> {
//                Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();
        });

        return itemView;
    }

    @Override
    public void destroyItem( ViewGroup container, int position,  Object object) {
        container.removeView((LinearLayout) object);
    }
}

package com.derby.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.derby.R;
import com.derby.models.stadium.Datum;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class StadiumViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Datum> modelList;

    private OnItemClickListener mItemClickListener;


    public StadiumViewAdapter(Context context, List<Datum> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(ArrayList<Datum> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_stadium_list, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final Datum model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;
            Glide.with(genericViewHolder.stadiumImage.getContext()).load(model.getImageUrl().get(0)).into(genericViewHolder.stadiumImage);
            genericViewHolder.stadiumName.setText(model.getName());
            genericViewHolder.stadiumPrice.setText(model.getPrice());
            genericViewHolder.stadiumAddress.setText(model.getAddress());
        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private Datum getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, Datum model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.stadium_image)
        ImageView stadiumImage;
        @BindView(R.id.stadium_name)
        TextView stadiumName;
        @BindView(R.id.stadium_price)
        TextView stadiumPrice;
        @BindView(R.id.stadium_address)
        TextView stadiumAddress;

        public ViewHolder(final View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);


            itemView.setOnClickListener(view -> mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition())));

        }
    }

}


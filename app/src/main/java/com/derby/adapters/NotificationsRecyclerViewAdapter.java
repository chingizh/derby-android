package com.derby.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.derby.R;
import com.derby.SessionKeys;
import com.derby.models.notifications.Datum;

import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class NotificationsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Datum> modelList;

    private OnItemClickListener mItemClickListener;

    public NotificationsRecyclerViewAdapter(Context context, List<Datum> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(List<Datum> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_notifications_recycler_list, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final Datum model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;

            if (!model.getFromUserPhotoFilePath().isEmpty())
                Glide.with(genericViewHolder.userImage.getContext()).load(model.getFromUserPhotoFilePath()).into(genericViewHolder.userImage);
            else
                Glide.with(genericViewHolder.userImage.getContext()).load(R.drawable.default_profilephoto).into(genericViewHolder.userImage);
            String currentlang = SessionManager.getString(SessionKeys.APP_LANG.getKey(), "");
                if (currentlang.equals("az"))
                genericViewHolder.whoAccepted.setText(model.getContent().getAz());
            else if (currentlang.equals("en"))
                genericViewHolder.whoAccepted.setText(model.getContent().getEn());
            else if (currentlang.equals("ru"))
                genericViewHolder.whoAccepted.setText(model.getContent().getRu());
            genericViewHolder.time.setText(model.getDate());
        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private Datum getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, Datum model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_image)
        CircleImageView userImage;
        @BindView(R.id.who_accepted)
        TextView whoAccepted;
        @BindView(R.id.time)
        TextView time;

        public ViewHolder(final View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(view -> mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition())));

        }
    }

}


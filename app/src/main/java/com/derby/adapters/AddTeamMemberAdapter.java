package com.derby.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.derby.R;
import com.derby.models.add_team_member.Datum;

import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class AddTeamMemberAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Datum> modelList;

    private OnItemClickListener mItemClickListener;


    public AddTeamMemberAdapter(Context context, List<Datum> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(List<Datum> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_team_member_list, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final Datum model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;

//            genericViewHolder.itemTxtTitle.setText(model.getTitle());
//            genericViewHolder.itemTxtMessage.setText(model.getMessage());
            if (!model.getPhotoFilePath().isEmpty()) {
                Glide.with(genericViewHolder.userImage.getContext()).load(model.getPhotoFilePath()).into(genericViewHolder.userImage);
            } else {
                Glide.with(genericViewHolder.userImage.getContext()).load(R.drawable.default_profilephoto).into(genericViewHolder.userImage);
            }
            genericViewHolder.teamName.setText(model.getFullname());
            genericViewHolder.teamLeaderOrEmail.setText(model.getEmail());

        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private Datum getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, Datum model);
        void onMessageClick(View view, int position, Datum model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_image)
        CircleImageView userImage;
        @BindView(R.id.team_name)
        TextView teamName;
        @BindView(R.id.team_leader_or_email)
        TextView teamLeaderOrEmail;
        @BindView(R.id.message)
        ImageView message;
        @BindView(R.id.imageView9)
        ImageView selected;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(view -> mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition())));

            message.setOnClickListener(view -> mItemClickListener.onMessageClick(message, getAdapterPosition(), modelList.get(getAdapterPosition())));

        }
    }

}


package com.derby.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.derby.R;
import com.derby.SessionKeys;
import com.derby.models.games.Datum;

import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class AllGamesRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Datum> modelList;

    private OnItemClickListener mItemClickListener;


    public AllGamesRecyclerViewAdapter(Context context, List<Datum> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(List<Datum> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recycler_listtttt, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final Datum model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;

            if (!model.getCreateTeam().getLogoUrl().isEmpty())
                Glide.with(genericViewHolder.teamLogo.getContext()).load(model.getCreateTeam().getLogoUrl()).into(genericViewHolder.teamLogo);

            String currentlang = SessionManager.getString(SessionKeys.APP_LANG.getKey(), "");
            if (currentlang.equals("az"))
                genericViewHolder.teamLocation.setText(model.getAddress().getValue().getAz());
            else if (currentlang.equals("en"))
                genericViewHolder.teamLocation.setText(model.getAddress().getValue().getEn());
            else if (currentlang.equals("ru"))
                genericViewHolder.teamLocation.setText(model.getAddress().getValue().getRu());
            genericViewHolder.teamSearhcing.setText(model.getCreateTeam().getName());
            genericViewHolder.teamWhen.setText(model.getStartDay());
            if (model.getCreateUserId() == SessionManager.getInt(SessionKeys.USER_ID.getKey(), 0))
                genericViewHolder.team_own_game.setVisibility(View.VISIBLE);
            else
                genericViewHolder.team_own_game.setVisibility(View.GONE);

        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private Datum getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, Datum model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.team_logo)
        CircleImageView teamLogo;
        @BindView(R.id.team_own_game)
        ImageView team_own_game;
        @BindView(R.id.team_searhcing)
        TextView teamSearhcing;
        @BindView(R.id.team_location)
        TextView teamLocation;
        @BindView(R.id.team_when)
        TextView teamWhen;

        public ViewHolder(final View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);


            itemView.setOnClickListener(view -> mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition())));

        }
    }

}


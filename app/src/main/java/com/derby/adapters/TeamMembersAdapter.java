package com.derby.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.derby.R;
import com.derby.models.team.Player;

import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class TeamMembersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private String mHeaderTitle;

    private OnHeaderClickListener mHeaderClickListener;

    private Context mContext;
    private List<Player> modelList;

    private OnItemClickListener mItemClickListener;
    private OnItemLongClickListener mItemOnLongClickListener;
    private OnMessageItemClickListener mMessageItemClickListener;


    public TeamMembersAdapter(Context context, List<Player> modelList, String headerTitle) {
        this.mContext = context;
        this.modelList = modelList;
        this.mHeaderTitle = headerTitle;
    }

    public void updateList(List<Player> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_header, parent, false);
            return new HeaderViewHolder(v);
        } else if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_team_members_list, parent, false);
            return new ViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;

//            headerHolder.txtTitleHeader.setText(mHeaderTitle);

        } else if (holder instanceof ViewHolder) {
            final Player model = getItem(position - 1);
            ViewHolder genericViewHolder = (ViewHolder) holder;
            if (model.getUser().getPhotoFilePath().isEmpty())
                Glide.with(genericViewHolder.userImage.getContext()).load(R.drawable.default_profilephoto).into(genericViewHolder.userImage);
            else
                Glide.with(genericViewHolder.userImage.getContext()).load(model.getUser().getPhotoFilePath()).into(genericViewHolder.userImage);
            if (model.getTeamCapitan()) {
                genericViewHolder.teamLeaderOrEmail.setText(genericViewHolder.teamLeaderOrEmail.getContext().getString(R.string.team_leader));
                genericViewHolder.teamLeaderOrEmail.setTextColor(Color.parseColor("#06A795"));
                genericViewHolder.teamLeader.setImageResource(R.drawable.ic_team_leader);
            } else {
                genericViewHolder.teamLeader.setImageResource(R.drawable.ic_message);
                genericViewHolder.teamLeaderOrEmail.setText(model.getUser().getEmail());
            }
            genericViewHolder.teamName.setText(model.getUser().getFullname());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }


    @Override
    public int getItemCount() {

        return modelList.size() + 1;
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void SetOnHeaderClickListener(final OnHeaderClickListener headerClickListener) {
        this.mHeaderClickListener = headerClickListener;
    }

    public void SetOnItemLongClickListener(final OnItemLongClickListener mItemOnLongClickListener) {
        this.mItemOnLongClickListener = mItemOnLongClickListener;
    }

    public void SetOnMessageItemClickListener(final OnMessageItemClickListener mMessageItemClickListener) {
        this.mMessageItemClickListener = mMessageItemClickListener;
    }

    private Player getItem(int position) {
        return modelList.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, Player model);
    }

    public interface OnItemLongClickListener {
        boolean onItemLongClicked(View view, int position, Player model);
    }

    public interface OnMessageItemClickListener {
        void onItemClick(View view, int position, Player model);
    }

    public interface OnHeaderClickListener {
        void onHeaderClick(View view, String headerTitle);
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
//        TextView txtTitleHeader;

        public HeaderViewHolder(final View itemView) {
            super(itemView);
//            this.txtTitleHeader = (TextView) itemView.findViewById(R.id.txt_header);


            itemView.setOnClickListener(view -> mHeaderClickListener.onHeaderClick(itemView, mHeaderTitle));

        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_image)
        CircleImageView userImage;
        @BindView(R.id.team_name)
        TextView teamName;
        @BindView(R.id.team_leader_or_email)
        TextView teamLeaderOrEmail;
        @BindView(R.id.team_leader)
        ImageView teamLeader;

        public ViewHolder(final View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(view -> mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition() - 1)));
            itemView.setOnLongClickListener(view -> mItemOnLongClickListener.onItemLongClicked(itemView, getAdapterPosition(), modelList.get(getAdapterPosition() - 1)));
            teamLeader.setOnClickListener(view -> mMessageItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition() - 1)));

        }
    }

}


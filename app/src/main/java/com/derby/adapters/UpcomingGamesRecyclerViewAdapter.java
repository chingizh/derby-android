package com.derby.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.derby.R;
import com.derby.SessionKeys;
import com.derby.models.games.Datum;

import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class UpcomingGamesRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Datum> modelList;

    private OnItemClickListener mItemClickListener;


    public UpcomingGamesRecyclerViewAdapter(Context context, List<Datum> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(List<Datum> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_upcoming_games_recycler_list, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final Datum model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;

            genericViewHolder.itemTime.setText(model.getStartTime());
            genericViewHolder.itemDate.setText(model.getStartDay());
            if (!model.getCreateTeam().getLogoUrl().isEmpty())
                Glide.with(genericViewHolder.itemTeamLogo.getContext()).load(model.getCreateTeam().getLogoUrl()).into(genericViewHolder.itemTeamLogo);

            String currentlang = SessionManager.getString(SessionKeys.APP_LANG.getKey(), "");
            if (currentlang.equals("az"))
                genericViewHolder.itemLocation.setText(model.getAddress().getValue().getAz());
            else if (currentlang.equals("en"))
                genericViewHolder.itemLocation.setText(model.getAddress().getValue().getEn());
            else if (currentlang.equals("ru"))
                genericViewHolder.itemLocation.setText(model.getAddress().getValue().getRu());
            genericViewHolder.itemTeamName.setText(model.getCreateTeam().getName());


        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private Datum getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, Datum model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_team_name)
        TextView itemTeamName;
        @BindView(R.id.item_location)
        TextView itemLocation;
        @BindView(R.id.item_time)
        TextView itemTime;
        @BindView(R.id.item_date)
        TextView itemDate;
        @BindView(R.id.item_team_logo)
        CircleImageView itemTeamLogo;

        public ViewHolder(final View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(view -> mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition())));

        }
    }

}


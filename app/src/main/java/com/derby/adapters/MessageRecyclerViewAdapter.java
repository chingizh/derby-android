package com.derby.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.derby.R;
import com.derby.models.messages.Datum;

import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by chingizh on 6/18/19 long live Chingiz!
 */
public class MessageRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Datum> modelList;

    private OnItemClickListener mItemClickListener;
    private OnItemLongClickListener mItemLongClickListener;

    public MessageRecyclerViewAdapter(Context context, List<Datum> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(List<Datum> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_messages_recycler_list, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final Datum model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;


            if (model.getType().getValue().getAz().contains("Qrup")) {
                if (!model.getPhotoFilePath().isEmpty())
                    Glide.with(genericViewHolder.imgUser.getContext()).load(model.getPhotoFilePath()).into(genericViewHolder.imgUser);
                else
                    Glide.with(genericViewHolder.imgUser.getContext()).load(R.drawable.derby_teamlogotem_2).into(genericViewHolder.imgUser);
            } else {
                if (!model.getPhotoFilePath().isEmpty())
                    Glide.with(genericViewHolder.imgUser.getContext()).load(model.getPhotoFilePath()).into(genericViewHolder.imgUser);
                else
                    Glide.with(genericViewHolder.imgUser.getContext()).load(R.drawable.default_profilephoto).into(genericViewHolder.imgUser);
            }
            genericViewHolder.itemFullname.setText(model.getName());
            genericViewHolder.itemMessage.setText(model.getLastMessage());
            genericViewHolder.itemTime.setText(model.getLastMessageDate());
            if (model.getUnreadMessageCount() != 0) {
                genericViewHolder.itemCount.setVisibility(View.VISIBLE);
                genericViewHolder.itemCount.setText(String.valueOf(model.getUnreadMessageCount()));
            }
            else
                genericViewHolder.itemCount.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void SetOnItemLongClickListener(final OnItemLongClickListener mItemLongClickListener){
        this.mItemLongClickListener = mItemLongClickListener;
    }

    private Datum getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, Datum model);
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(View view, int position, Datum model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_user)
        CircleImageView imgUser;
        @BindView(R.id.item_fullname)
        TextView itemFullname;
        @BindView(R.id.item_message)
        TextView itemMessage;
        @BindView(R.id.item_time)
        TextView itemTime;
        @BindView(R.id.item_count)
        TextView itemCount;

        public ViewHolder(final View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);


            itemView.setOnClickListener(view -> mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition())));
            itemView.setOnLongClickListener(view -> {
                mItemLongClickListener.onItemLongClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));
                return true;
            });

        }
    }
}

package com.derby;

/**
 * Created by chingizh on 6/17/19 long live Chingiz!
 */
public enum SessionKeys {
    PREFS_NAME("test"),
    TOKEN("token"),
    FIRST_TIME("first_time"),
    USER_ID("user_id"),
    USER_FULLNAME("user_fullname"),
    USER_EMAIL("user_email"),
    ADDRESS("address"),
    ADDRESS_ID("address_id"),
    TEAM_ID("team_id"),
    APP_LANG("app_lang");

    private String key;

    SessionKeys(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}

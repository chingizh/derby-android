package com.derby;

/**
 * Created by chingizh on 2019-07-06 long live Chingiz!
 */
public class StompSingleton {

    private static StompSingleton instance;

    private StompSingleton() {}

    static {
        try {
            instance = new StompSingleton();
        } catch (Exception e) {
            throw new RuntimeException("Exception occured in creating singleton instance");
        }
    }

    public static StompSingleton getInstance() {
        return instance;
    }


}

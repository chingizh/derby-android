package com.derby.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.login.LoginActivity;
import com.franmontiel.localechanger.LocaleChanger;

import androidx.annotation.Nullable;
import hundredthirtythree.sessionmanager.SessionManager;

/**
 * Created by chingizh on 5/6/19 long live Chingiz!
 */
public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        int SPLASH_DISPLAY_LENGTH = 1000; //TODO change to 5000 after finishing
        new Handler().postDelayed(() -> {
            /* Create an Intent that will start the Menu-Activity. */
            if (SessionManager.getString(SessionKeys.TOKEN.getKey(), "").isEmpty()) {
                Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                SplashActivity.this.startActivity(mainIntent);
            } else {
                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
            }
            SplashActivity.this.finish();
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }

}

package com.derby.activities.team;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.activities.messages.ChatActivity;
import com.derby.adapters.AddTeamMemberAdapter;
import com.derby.adapters.SelectedTeamMemberAdapter;
import com.derby.models.add_team_member.AddTeamMember;
import com.derby.models.add_team_member.Datum;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddTeamMemberActivityy extends BaseActivity {

    @BindView(R.id.selected)
    TextView selected;
    @BindView(R.id.recycler_view_selected_members)
    RecyclerView recyclerViewSelectedMembers;
    @BindView(R.id.btn_done)
    Button btnDone;
    @BindView(R.id.layout_done_btn)
    RelativeLayout layoutDoneBtn;

    private RecyclerView recyclerView;

    // @BindView(R.id.recycler_view)
    // RecyclerView recyclerView;

    //@BindView(R.id.toolbar)
    //Toolbar toolbar;
    private Toolbar toolbar;

    // @BindView(R.id.swipe_refresh_recycler_list)
    // SwipeRefreshLayout swipeRefreshRecyclerList;

    //    private SwipeRefreshLayout swipeRefreshRecyclerList;
    private AddTeamMemberAdapter mAdapter;
    private SelectedTeamMemberAdapter selectedTeamMemberAdapter;

    private List<Datum> modelList = new ArrayList<>();
    private List<Datum> selectedMembers = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_team_member_activityy);
        ButterKnife.bind(this);
        findViews();
        initToolbar(getString(R.string.add_member));

//        swipeRefreshRecyclerList.setOnRefreshListener(() -> {
//
//            // Do your stuff on refresh
//            new Handler().postDelayed(() -> {
//
//                if (swipeRefreshRecyclerList.isRefreshing())
//                    swipeRefreshRecyclerList.setRefreshing(false);
//            }, 5000);
//
//        });
        getOtherTeamMemberToAdd();
    }

    private void findViews() {
        toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.recycler_view);
//        swipeRefreshRecyclerList = findViewById(R.id.swipe_refresh_recycler_list);
    }

    public void initToolbar(String title) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_search, menu);


        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat
                .getActionView(menu.findItem(R.id.action_search));

        SearchManager searchManager = (SearchManager) this.getSystemService(this.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));

        //changing edittext color
        EditText searchEdit = searchView.findViewById(R.id.search_src_text);
        searchEdit.setTextColor(Color.WHITE);
        searchEdit.setHintTextColor(Color.WHITE);
        searchEdit.setBackgroundColor(Color.TRANSPARENT);
        searchEdit.setHint(getString(R.string.search));


        InputFilter[] fArray = new InputFilter[2];
        fArray[0] = new InputFilter.LengthFilter(40);
        fArray[1] = (source, start, end, dest, dstart, dend) -> {
            for (int i = start; i < end; i++) {
                if (!Character.isLetterOrDigit(source.charAt(i)))
                    return "";
            }
            return null;
        };
        searchEdit.setFilters(fArray);
        View v = searchView.findViewById(R.id.search_plate);
        v.setBackgroundColor(Color.TRANSPARENT);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                List<Datum> filterList = new ArrayList<>();
                if (s.length() > 0) {
                    for (int i = 0; i < modelList.size(); i++) {
                        if (modelList.get(i).getFullname().toLowerCase().contains(s.toString().toLowerCase())) {
                            filterList.add(modelList.get(i));
                            mAdapter.updateList(filterList);
                        }
                    }
                } else {
                    mAdapter.updateList(modelList);
                }
                return false;
            }
        });
        return true;
    }

    private void processQuery(String query) {
        // in real app you'd have it instantiated just once
        ArrayList<Datum> result = new ArrayList<>();

        // case insensitive search
        for (Datum member : result) {
            if (member.getFullname().toLowerCase().contains(query.toLowerCase())) {
                result.add(member);
            }
        }

        mAdapter.updateList(result);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void setAdapter(List<Datum> modelList) {
        mAdapter = new AddTeamMemberAdapter(AddTeamMemberActivityy.this, modelList);

//        recyclerView.setHasFixedSize(true);

        // use a linear layout manager

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);


        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(AddTeamMemberActivityy.this, R.drawable.divider_recyclerview));
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.setAdapter(mAdapter);

        mAdapter.SetOnItemClickListener(new AddTeamMemberAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Datum model) {
                //handle item click events here
                ImageView selectedImage = view.findViewById(R.id.imageView9);
                if (!selectedMembers.contains(model)) {
                    selectedMembers.add(model);
                    selectedImage.setVisibility(View.VISIBLE);
                }
                setSelectedTeamMemberAdapter(selectedMembers);
                selected.setText(String.format(getString(R.string.selected_s), String.valueOf(selectedMembers.size())));
                selected.setVisibility(View.VISIBLE);
                layoutDoneBtn.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMessageClick(View view, int position, Datum model) {
                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra("title", model.getFullname());
                intent.putExtra("userId", model.getId());
                intent.putExtra("type", "Private");
                intent.putExtra("conversID", model.getId());
                startActivity(intent);
            }
        });
    }

    private void setSelectedTeamMemberAdapter(List<Datum> modelList) {
        selectedTeamMemberAdapter = new SelectedTeamMemberAdapter(AddTeamMemberActivityy.this, modelList);

        recyclerViewSelectedMembers.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewSelectedMembers.setLayoutManager(layoutManager);


        recyclerViewSelectedMembers.setAdapter(selectedTeamMemberAdapter);


        selectedTeamMemberAdapter.SetOnItemClickListener((view, position, model) -> {

            //handle item click events here
//                Toast.makeText(getApplicationContext(), "Hey " + model.getTitle(), Toast.LENGTH_SHORT).show();
            if (selectedMembers.size() != 0) {
                selectedMembers.remove(model);
                setSelectedTeamMemberAdapter(selectedMembers);
            } else {
                recyclerViewSelectedMembers.setAdapter(null);
            }
            selected.setText(String.format(getString(R.string.selected_s), String.valueOf(selectedMembers.size())));
            if (selectedMembers.size() != 0) {
                selected.setVisibility(View.VISIBLE);
                layoutDoneBtn.setVisibility(View.VISIBLE);
            } else {
                selected.setVisibility(View.GONE);
                layoutDoneBtn.setVisibility(View.GONE);
            }
        });
    }

    private void getOtherTeamMemberToAdd() {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<AddTeamMember> call = APIUtils.getAPI().otherUserList(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<AddTeamMember>() {
            @Override
            public void onResponse(Call<AddTeamMember> call, Response<AddTeamMember> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        modelList = response.body().getData();
                        setAdapter(modelList);
                    }
                }
            }

            @Override
            public void onFailure(Call<AddTeamMember> call, Throwable t) {

            }
        });
    }

    private void addMembersToTeam() {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        int[] membrs = new int[selectedMembers.size()];
        for (int i = 0; i < selectedMembers.size(); i++) {
            membrs[i] = selectedMembers.get(i).getId();
        }
        body.put("userId", membrs);
        Call<String> call = APIUtils.getAPI().addMember(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.btn_done)
    public void onViewClicked() {
        addMembersToTeam();
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

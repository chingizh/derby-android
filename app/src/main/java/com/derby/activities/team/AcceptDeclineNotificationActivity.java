package com.derby.activities.team;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.derby.EmptyRecyclerView;
import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.adapters.AcceptDeclineTeamViewAdapter;
import com.derby.models.teamrequest.Datum;
import com.derby.models.teamrequest.TeamRequest;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-06-30 long live Chingiz!
 */
public class AcceptDeclineNotificationActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    EmptyRecyclerView recyclerView;
    @BindView(R.id.empty_view)
    TextView emptyView;

    private AcceptDeclineTeamViewAdapter mAdapter;

    private List<Datum> modelList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_decline_notification);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.team_requests));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getNotification();
    }

    private void getNotification() {
        Call<TeamRequest> call = APIUtils.getAPI().teamRequestList(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<TeamRequest>() {
            @Override
            public void onResponse(Call<TeamRequest> call, Response<TeamRequest> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        modelList = response.body().getData();
                        if (modelList.size() == 0) {
                            modelList.clear();
                            recyclerView.setEmptyView(findViewById(R.id.empty_view));
                        } else if (modelList.size() != 0)
                            setAdapter(modelList);
                    }
                }
            }

            @Override
            public void onFailure(Call<TeamRequest> call, Throwable t) {

            }
        });
    }

    private void setAdapter(List<Datum> modelList) {

        mAdapter = new AcceptDeclineTeamViewAdapter(getApplicationContext(), modelList);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener(new AcceptDeclineTeamViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Datum model) {

            }

            @Override
            public void onAcceptClick(View view, int position, Datum model) {
                acceptTeam(model.getId());
            }

            @Override
            public void onDeclineClick(View view, int position, Datum model) {
                declineTeam(model.getId());
            }
        });
    }

    private void acceptTeam(int id) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().acceptTeam(id, body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        Toast.makeText(AcceptDeclineNotificationActivity.this, getApplicationContext().getString(R.string.accepted_game), Toast.LENGTH_SHORT).show();
                        getNotification();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void declineTeam(int id) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().declineTeam(id, body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        Toast.makeText(AcceptDeclineNotificationActivity.this, getApplicationContext().getString(R.string.declined_game), Toast.LENGTH_SHORT).show();
                        getNotification();
                    }

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

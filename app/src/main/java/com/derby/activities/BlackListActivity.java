package com.derby.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.derby.R;
import com.derby.SessionKeys;
import com.derby.models.blacklist.BlackList;
import com.derby.models.blacklist.Datum;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import de.hdodenhof.circleimageview.CircleImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-07-13 long live Chingiz!
 */
public class BlackListActivity extends BaseActivity {

    @BindView(R.id.listViewBlackList)
    ListView listViewBlackList;

    private List<Datum> users;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_black_list);
        ButterKnife.bind(this);
        setTitle(getString(R.string.black_list));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getBlackList();
    }

    private void getBlackList() {
        Call<BlackList> call = APIUtils.getAPI().blackList(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<BlackList>() {
            @Override
            public void onResponse(Call<BlackList> call, Response<BlackList> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        users = response.body().getData();
                        if (users.size() != 0) {
                            UsersAdapter adapter = new UsersAdapter(getApplicationContext(), users);
                            listViewBlackList.setAdapter(adapter);
                        } else
                            listViewBlackList.setEmptyView(findViewById(R.id.emptyElement));
                    }
                }
            }

            @Override
            public void onFailure(Call<BlackList> call, Throwable t) {

            }
        });

        listViewBlackList.setOnItemLongClickListener((adapterView, view, i, l) -> {
            new AlertDialog.Builder(BlackListActivity.this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(getString(R.string.are_u_sure_to_remove_user_blacklist))
                    .setPositiveButton(getString(R.string.yes), (dialog, which) -> removeFromBlackList(users.get(i).getId()))
                    .setNegativeButton(getString(R.string.no), null)
                    .show();
            return false;
        });
    }

    private void removeFromBlackList(int id) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().removeUserFromBlackList(id, body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        Toast.makeText(BlackListActivity.this, getString(R.string.user_removed_blacklist), Toast.LENGTH_SHORT).show();
                        users.clear();
                        getBlackList();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }

    public static class UsersAdapter extends ArrayAdapter<Datum> {

        public UsersAdapter(Context context, List<Datum> users) {
            super(context, R.layout.item_team_member_list, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            Datum user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            ViewHolder viewHolder; // view lookup cache stored in tag
            if (convertView == null) {
                // If there's no view to re-use, inflate a brand new view for row
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.item_team_member_list, parent, false);
                viewHolder = new ViewHolder(convertView);
                viewHolder.teamName.setText(user.getUser().getFullname());
                viewHolder.teamLeaderOrEmail.setText(user.getUser().getEmail());
                viewHolder.message.setVisibility(View.GONE);
                if (user.getUser().getPhotoFilePath() != null)
                    Glide.with(viewHolder.userImage.getContext()).load(user.getUser().getPhotoFilePath()).into(viewHolder.userImage);
                // Cache the viewHolder object inside the fresh view
                convertView.setTag(viewHolder);
            } else {
                // View is being recycled, retrieve the viewHolder object from tag
                viewHolder = (ViewHolder) convertView.getTag();
            }
            return convertView;
        }

        class ViewHolder {
            @BindView(R.id.user_image)
            CircleImageView userImage;
            @BindView(R.id.team_name)
            TextView teamName;
            @BindView(R.id.team_leader_or_email)
            TextView teamLeaderOrEmail;
            @BindView(R.id.message)
            ImageView message;

            ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }
    }
}

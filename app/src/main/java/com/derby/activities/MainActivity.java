package com.derby.activities;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.game.CreateGameActivity;
import com.derby.activities.messages.NewMessageActivity;
import com.derby.activities.settings.SettingsActivity;
import com.derby.activities.team.TeamEditActivity;
import com.derby.fragments.GamesFragment;
import com.derby.fragments.MessageFragment;
import com.derby.fragments.NotificationFragment;
import com.derby.fragments.StadiumFragment;
import com.derby.fragments.TeamFragment;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.HashMap;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class MainActivity extends BaseActivity {

    @BindView(R.id.main_toolbar)
    ConstraintLayout mainToolbar;
    @BindView(R.id.my_team_toolbar)
    ConstraintLayout myTeamToolbar;
    @BindView(R.id.my_stadium_toolbar)
    ConstraintLayout myStadiumToolbar;
    @BindView(R.id.my_notifications_toolbar)
    ConstraintLayout myNotificationToolbar;
    @BindView(R.id.my_messages_toolbar)
    ConstraintLayout myMessagesToolbar;
    private BottomNavigationView navigation;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_games:
                changeFragment(new GamesFragment());
                changeToolbar(0);
                return true;
            case R.id.navigation_team:
                changeFragment(new TeamFragment());
                changeToolbar(1);
                return true;
            case R.id.navigation_chat:
                changeFragment(new MessageFragment());
                changeToolbar(2);
                return true;
            case R.id.navigation_notifications:
                changeFragment(new NotificationFragment());
                changeToolbar(5);
                return true;
            case R.id.navigation_stadiums:
                changeFragment(new StadiumFragment());
                changeToolbar(6);
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        navigation = findViewById(R.id.navigation);
//        removeShiftMode(navigation);
//        navigation.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (SessionManager.getBoolean(SessionKeys.FIRST_TIME.getKey(), false)) {
            changeFragment(new TeamFragment());
            changeToolbar(1);
            navigation.getMenu().findItem(R.id.navigation_team).setChecked(true);
        } else {
            changeFragment(new GamesFragment());
            changeToolbar(0);
        }
    }

    private void changeToolbar(int i) {
        if (i == 0) {
            mainToolbar.setVisibility(View.VISIBLE);
            myTeamToolbar.setVisibility(GONE);
            myStadiumToolbar.setVisibility(GONE);
            myNotificationToolbar.setVisibility(GONE);
            myMessagesToolbar.setVisibility(GONE);
        } else if (i == 1) {
            mainToolbar.setVisibility(GONE);
            myTeamToolbar.setVisibility(View.VISIBLE);
            myStadiumToolbar.setVisibility(GONE);
            myNotificationToolbar.setVisibility(GONE);
            myMessagesToolbar.setVisibility(GONE);
        } else if (i == 2) {
            mainToolbar.setVisibility(GONE);
            myTeamToolbar.setVisibility(GONE);
            myStadiumToolbar.setVisibility(GONE);
            myNotificationToolbar.setVisibility(GONE);
            myMessagesToolbar.setVisibility(View.VISIBLE);
        } else if (i == 5) {
            mainToolbar.setVisibility(GONE);
            myTeamToolbar.setVisibility(GONE);
            myStadiumToolbar.setVisibility(GONE);
            myNotificationToolbar.setVisibility(View.VISIBLE);
            myMessagesToolbar.setVisibility(GONE);
        } else if (i == 6) {
            mainToolbar.setVisibility(GONE);
            myTeamToolbar.setVisibility(GONE);
            myStadiumToolbar.setVisibility(View.VISIBLE);
            myNotificationToolbar.setVisibility(GONE);
            myMessagesToolbar.setVisibility(GONE);
        }
    }

    @SuppressLint("RestrictedApi")
    static void removeShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
            //noinspection RestrictedApi
            item.setShifting(false);
            item.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);

            // set once again checked value, so view will be updated
            //noinspection RestrictedApi
            item.setChecked(item.getItemData().isChecked());
        }
    }

    @OnClick({R.id.main_settings, R.id.main_create_game, R.id.main_new_message, R.id.team_delete})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.main_settings:
                startNewActivity(MainActivity.this, SettingsActivity.class);
                break;
            case R.id.main_create_game:
                startNewActivity(MainActivity.this, CreateGameActivity.class);
                break;
            case R.id.main_new_message:
                startNewActivity(MainActivity.this, NewMessageActivity.class);
                break;
            case R.id.team_delete:
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(MainActivity.this, view);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_team, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.action_edit_team:
                            Intent i = new Intent(this, TeamEditActivity.class);
                            i.putExtra("teamId", SessionManager.getInt(SessionKeys.TEAM_ID.getKey(), 0));
                            startActivity(i);
                            break;
                        case R.id.action_delete_team:
                            new AlertDialog.Builder(this)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setMessage(getApplicationContext().getString(R.string.are_u_sure_team_delete))
                                    .setPositiveButton(getApplicationContext().getString(R.string.yes), (dialog, which) -> deleteTeam())
                                    .setNegativeButton(getApplicationContext().getString(R.string.no), null)
                                    .show();
                            break;
                    }
                    return true;
                });

                popup.show();//showing popup menu
                break;
        }
    }

    private void deleteTeam() {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().deleteTeam(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        changeFragment(new TeamFragment());
                        changeToolbar(1);
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

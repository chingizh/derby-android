package com.derby.activities;

import android.content.Context;
import android.content.Intent;

import com.derby.LanguageHelper;
import com.derby.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

/**
 * Created by chingizh on 5/6/19 long live Chingiz!
 */
public class BaseActivity extends AppCompatActivity {

    public void changeFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.frame, fragment).commit();
    }

    public void startNewActivity(Context packageContext, Class<?> cls) {
        Intent i = new Intent(packageContext, cls);
        startActivity(i);
    }
}

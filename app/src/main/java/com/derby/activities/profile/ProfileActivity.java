package com.derby.activities.profile;

import com.google.android.material.textfield.TextInputEditText;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.franmontiel.localechanger.LocaleChanger;

import de.hdodenhof.circleimageview.CircleImageView;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;

/**
 * Created by chingizh on 2019-07-04 long live Chingiz!
 */
public class ProfileActivity extends BaseActivity {

    @BindView(R.id.logo)
    CircleImageView logo;
    @BindView(R.id.input_fullname)
    TextInputEditText inputFullname;
    @BindView(R.id.input_email)
    TextInputEditText inputEmail;
    @BindView(R.id.input_password)
    TextInputEditText inputPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.game_detail));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setViews();
    }

    private void setViews() {
        inputFullname.setText(SessionManager.getString(SessionKeys.USER_FULLNAME.getKey(), ""));
        inputEmail.setText(SessionManager.getString(SessionKeys.USER_EMAIL.getKey(), ""));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.input_password, R.id.btn_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.input_password:
                break;
            case R.id.btn_save:
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

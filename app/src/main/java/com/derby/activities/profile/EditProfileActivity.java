package com.derby.activities.profile;

import com.google.android.material.textfield.TextInputEditText;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.derby.FileUtil;
import com.derby.R;
import com.derby.RealPathUtil;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.models.profile.Profile;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import de.hdodenhof.circleimageview.CircleImageView;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gun0912.tedimagepicker.builder.TedImagePicker;
import gun0912.tedimagepicker.builder.listener.OnSelectedListener;
import hundredthirtythree.sessionmanager.SessionManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-07-10 long live Chingiz!
 */
public class EditProfileActivity extends BaseActivity {
    @BindView(R.id.user_image)
    CircleImageView userImage;
    @BindView(R.id.input_fullname)
    TextInputEditText inputFullname;
    @BindView(R.id.input_email)
    TextInputEditText inputEmail;
    @BindView(R.id.input_password)
    TextInputEditText inputPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.profile));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getProfielInfo();

        permissionCheck();
    }

    private void permissionCheck() {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
//                Toast.makeText(EditProfileActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
//                Toast.makeText(EditProfileActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }


        };
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    private void getProfielInfo() {
        Call<Profile> call = APIUtils.getAPI().meProfile(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        inputFullname.setText(response.body().getData().getFullname());
                        inputEmail.setText(response.body().getData().getEmail());
                        if (!response.body().getData().getPhotoFilePath().isEmpty()) {
                            Glide.with(getApplicationContext()).load(response.body().getData().getPhotoFilePath()).into(userImage);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {

            }
        });
    }

    private void changeProfile() {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        body.put("fullname", inputFullname.getText().toString());
        body.put("email", inputEmail.getText().toString());
        Call<String> call = APIUtils.getAPI().changeProfile(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @OnClick({R.id.done, R.id.input_password, R.id.user_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.done:
                changeProfile();
                break;
            case R.id.input_password:
                startNewActivity(getApplicationContext(), ChangePasswordActivity.class);
                break;
            case R.id.user_image:
                chooseImage();
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void chooseImage() {
        TedImagePicker.with(this)
                .start(new OnSelectedListener() {
                    @Override
                    public void onSelected(@NotNull Uri uri) {
                        try {
                            File file = FileUtil.from(EditProfileActivity.this, uri);
                            uploadImage(file);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void uploadImage(File file) {
        RequestBody image = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), image);
        Call<String> call = APIUtils.getAPI().uploadFile(body, SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        Toast.makeText(EditProfileActivity.this, "yuklendi", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}

package com.derby.activities.profile;

import com.google.android.material.textfield.TextInputEditText;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.HashMap;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-07-10 long live Chingiz!
 */
public class ChangePasswordActivity extends BaseActivity {
    @BindView(R.id.input_current_passowrd)
    TextInputEditText inputCurrentPassowrd;
    @BindView(R.id.input_new_password)
    TextInputEditText inputNewPassword;
    @BindView(R.id.input_password_repeat)
    TextInputEditText inputPasswordRepeat;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.change_password));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.done)
    public void onViewClicked() {
        changeProfile();
    }

    private void changeProfile() {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        body.put("oldPassword", inputCurrentPassowrd.getText().toString());
        body.put("newPassword", inputNewPassword.getText().toString());
        body.put("confirmNewPassword", inputPasswordRepeat.getText().toString());
        Call<String> call = APIUtils.getAPI().changeProfile(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

package com.derby.activities.messages;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.adapters.AddTeamMemberAdapter;
import com.derby.models.add_team_member.AddTeamMember;
import com.derby.models.add_team_member.Datum;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-07-08 long live Chingiz!
 */
public class NewMessageActivity extends BaseActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);
        ButterKnife.bind(this);
        setTitle(getString(R.string.new_message));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getOtherTeamMemberToAdd();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private AddTeamMemberAdapter mAdapter;

    private void setAdapter(List<Datum> modelList) {
        mAdapter = new AddTeamMemberAdapter(NewMessageActivity.this, modelList);

//        recyclerView.setHasFixedSize(true);

        // use a linear layout manager

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);


        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(NewMessageActivity.this, R.drawable.divider_recyclerview));
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.setAdapter(mAdapter);

        mAdapter.SetOnItemClickListener(new AddTeamMemberAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Datum model) {
                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra("title", model.getFullname());
                intent.putExtra("userId", model.getId());
                intent.putExtra("type", "Private");
                intent.putExtra("conversID", model.getId());
                startActivity(intent);
            }

            @Override
            public void onMessageClick(View view, int position, Datum model) {
                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra("title", model.getFullname());
                intent.putExtra("userId", model.getId());
                intent.putExtra("type", "Private");
                intent.putExtra("conversID", model.getId());
                startActivity(intent);
            }
        });
    }

    private void getOtherTeamMemberToAdd() {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<AddTeamMember> call = APIUtils.getAPI().otherUserList(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<AddTeamMember>() {
            @Override
            public void onResponse(Call<AddTeamMember> call, Response<AddTeamMember> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        setAdapter(response.body().getData());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddTeamMember> call, Throwable t) {

            }
        });
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

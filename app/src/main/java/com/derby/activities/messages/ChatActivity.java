package com.derby.activities.messages;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.models.converstationMessages.ConverstationMessages;
import com.derby.models.converstationMessages.Datum;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;
import ua.naiksoftware.stomp.dto.StompHeader;

import static com.facebook.AccessTokenManager.TAG;

/**
 * Created by chingizh on 2019-07-04 long live Chingiz!
 */
public class ChatActivity extends BaseActivity {

    @BindView(R.id.chat_view)
    LinearLayout chatView;
    @BindView(R.id.input_message)
    TextInputEditText inputMessage;
    @BindView(R.id.input_layout_message)
    TextInputLayout inputLayoutMessage;
    @BindView(R.id.send_message)
    FloatingActionButton sendMessage;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.input_message1)
    EditText inputMessage1;

    private StompClient mStompClient;
    private CompositeDisposable compositeDisposable;

    private static String type;
    private static int conversID;
    private static int userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        setTitle(extras.getString("title"));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));

        type = extras.getString("type");
        conversID = extras.getInt("conversID");
        userId = extras.getInt("userId");

        if (type.equals("Qrup"))
            getGroupMessages(conversID);
        else getUserMessages(userId);

        mStompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, "http://134.209.231.73:8182/DerbyRest/chat");
        resetSubscriptions();
        connectStomp();

        inputMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                inputMessage.setHint("");
            }
        });

        sendMessage.getDrawable().mutate().setTint(ContextCompat.getColor(this, R.color.white));


//        getWindow().setBackgroundDrawableResource(R.drawable.chat_background);
    }

    public void connectStomp() {

        List<StompHeader> headers = new ArrayList<>();
        headers.add(new StompHeader("Auth", SessionManager.getString(SessionKeys.TOKEN.getKey(), "")));

//        mStompClient.withClientHeartbeat(1000).withServerHeartbeat(1000);

        resetSubscriptions();

        Disposable dispLifecycle = mStompClient.lifecycle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(lifecycleEvent -> {
                    switch (lifecycleEvent.getType()) {
                        case OPENED:
                            System.out.println("Stomp connection opened");
                            break;
                        case ERROR:
                            Log.e(TAG, "Stomp connection error", lifecycleEvent.getException());
                            System.out.println("Stomp connection error");
                            break;
                        case CLOSED:
                            System.out.println("Stomp connection closed");
                            resetSubscriptions();
                            break;
                        case FAILED_SERVER_HEARTBEAT:
                            System.out.println("Stomp failed server heartbeat");
                            break;
                    }
                });

        compositeDisposable.add(dispLifecycle);

        String sessionToken = SessionManager.getString(SessionKeys.TOKEN.getKey(), "");

        // Receive greetings
        Disposable dispTopic = mStompClient.topic("/topic/messages/" + sessionToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(topicMessage -> {
                    Log.d(TAG, "Received " + topicMessage.getPayload());
                    JSONObject chat = new JSONObject(topicMessage.getPayload());
                    JSONObject data = chat.getJSONObject("data");
                    String message = data.getString("message");
                    String createdAt = data.getString("createDate");
                    String senderName = data.getJSONObject("user").getString("fullname");
                    int conversationId = data.getJSONObject("conversation").getInt("id");
                    String position = data.getJSONObject("type").getJSONObject("value").getString("en");
                    if (conversationId == conversID)
                        if (position.equals("right"))
                            outgoingMessage(message, createdAt);
                        else
                            incomingMessage(message, createdAt, senderName);
                    scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));
                }, throwable -> {
                    Log.e(TAG, "Error on subscribe topic", throwable);
                });

        compositeDisposable.add(dispTopic);

        mStompClient.connect(headers);
    }

    public void disconnectStomp() {
        mStompClient.disconnect();
    }

    private void resetSubscriptions() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onDestroy() {
        disconnectStomp();

        if (compositeDisposable != null) compositeDisposable.dispose();
        super.onDestroy();
    }

    private void incomingMessage(String text, String timet, String senderName) {
        View layout = LayoutInflater.from(getApplicationContext()).inflate(R.layout.incoming_message_layout, null, false);
        TextView message = layout.findViewById(R.id.textview_message);
        TextView time = layout.findViewById(R.id.textview_time);
        TextView sender = layout.findViewById(R.id.textview_sender);
        message.setText(text);
        time.setText(timet);
        sender.setText(senderName);
        chatView.addView(layout);
    }

    private void outgoingMessage(String text, String timet) {
        View layout = LayoutInflater.from(getApplicationContext()).inflate(R.layout.outgoin_message_layout, null, false);
        TextView message = layout.findViewById(R.id.textview_message);
        TextView time = layout.findViewById(R.id.textview_time);
        message.setText(text);
        time.setText(timet);
        chatView.addView(layout);
    }

    private void getGroupMessages(int conversID) {
        Call<ConverstationMessages> call = APIUtils.getAPI().converstationBetweenGroupList(conversID, SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<ConverstationMessages>() {
            @Override
            public void onResponse(Call<ConverstationMessages> call, Response<ConverstationMessages> response) {
                if (response.isSuccessful()) {
                    List<Datum> messages = response.body().getData();
                    if (messages != null)
                        for (Datum message : messages) {
                            if (message.getUser().getId() == SessionManager.getInt(SessionKeys.USER_ID.getKey(), 0)) {
                                outgoingMessage(message.getMessage(), message.getCreateDate());
                            } else {
                                incomingMessage(message.getMessage(), message.getCreateDate(), message.getUser().getFullname());
                            }
                        }
                    scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));
                }
            }

            @Override
            public void onFailure(Call<ConverstationMessages> call, Throwable t) {

            }
        });
    }

    private void getUserMessages(int conversID) {
        Call<ConverstationMessages> call = APIUtils.getAPI().conversationBetweenUserList(conversID, SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<ConverstationMessages>() {
            @Override
            public void onResponse(Call<ConverstationMessages> call, Response<ConverstationMessages> response) {
                if (response.isSuccessful()) {
                    List<Datum> messages = response.body().getData();
                    for (Datum message : messages) {
                        if (message.getUser().getId() == SessionManager.getInt(SessionKeys.USER_ID.getKey(), 0)) {
                            outgoingMessage(message.getMessage(), message.getCreateDate());
                        } else {
                            incomingMessage(message.getMessage(), message.getCreateDate(), message.getUser().getFullname());
                        }
                    }
                    scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));
                }
            }

            @Override
            public void onFailure(Call<ConverstationMessages> call, Throwable t) {

            }
        });
    }

    private void writeMessageToGroup(int conversID) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        body.put("text", inputMessage1.getText().toString());
        Call<String> call = APIUtils.getAPI().sendMessageToGroup(body, conversID);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    inputMessage1.setText("");
                    sendMessage.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void writeMessageToPerson(int userId) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        body.put("text", inputMessage1.getText().toString());
        Call<String> call = APIUtils.getAPI().sendMessageToUser(body, userId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    inputMessage1.setText("");
                    sendMessage.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_chat, menu);

        final MenuItem block = menu.findItem(R.id.action_block);
        final MenuItem delete = menu.findItem(R.id.action_delete_chat);

        if (type.equals("Qrup"))
            block.setVisible(false);
        else
            block.setVisible(true);

        block.setOnMenuItemClickListener(menuItem -> {
            blockUser(userId);
            return false;
        });

        delete.setOnMenuItemClickListener(menuItem -> {
            deleteChat(conversID);
            return false;
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void blockUser(int id) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().addUserToBlackList(id, body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void deleteChat(int id) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().hideConversation(body, id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.send_message)
    public void onViewClicked() {
        if (!inputMessage1.getText().toString().isEmpty()) {
        if (type.equals("Qrup"))
            writeMessageToGroup(conversID);
        else writeMessageToPerson(userId);
            sendMessage.setEnabled(false);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

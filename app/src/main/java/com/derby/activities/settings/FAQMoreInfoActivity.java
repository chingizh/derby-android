package com.derby.activities.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.derby.R;
import com.derby.activities.BaseActivity;
import com.franmontiel.localechanger.LocaleChanger;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by chingizh on 2019-07-12 long live Chingiz!
 */
public class FAQMoreInfoActivity extends BaseActivity {
    @BindView(R.id.question)
    TextView question;
    @BindView(R.id.answer)
    TextView answer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_moreinfo);
        ButterKnife.bind(this);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle data = getIntent().getExtras();
        setTitle(data.getString("question"));

        question.setText(data.getString("question"));
        answer.setText(data.getString("answer"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }

    @OnClick({R.id.yes, R.id.no})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.yes:
                findViewById(R.id.no).setVisibility(View.GONE);
                break;
            case R.id.no:
                findViewById(R.id.yes).setVisibility(View.GONE);
                break;
        }
    }
}

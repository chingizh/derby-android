package com.derby.activities.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;

import com.derby.R;
import com.derby.activities.BaseActivity;
import com.franmontiel.localechanger.LocaleChanger;

import androidx.annotation.Nullable;
import butterknife.ButterKnife;

/**
 * Created by chingizh on 2019-07-12 long live Chingiz!
 */
public class ContactsActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.contacts));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

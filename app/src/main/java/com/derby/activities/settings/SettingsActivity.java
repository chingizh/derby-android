package com.derby.activities.settings;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.activities.BlackListActivity;
import com.derby.activities.SplashActivity;
import com.derby.activities.profile.EditProfileActivity;
import com.derby.models.profile.Profile;
import com.derby.rest.APIUtils;
import com.facebook.login.LoginManager;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.HashMap;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-07-02 long live Chingiz!
 */
public class SettingsActivity extends BaseActivity {

    @BindView(R.id.userFullName)
    TextView userFullName;
    @BindView(R.id.checkboxNewMatcthes)
    CheckBox checkboxNewMatcthes;
    @BindView(R.id.checkboxNewMessages)
    CheckBox checkboxNewMessages;
    @BindView(R.id.checkboxSupportMessages)
    CheckBox checkboxSupportMessages;
    @BindView(R.id.appLanguage)
    TextView appLanguage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.settings));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setViews();
    }

    private void setViews() {
        getProfielInfo();
        checkAppLang();

        checkboxNewMatcthes.setOnClickListener(view -> changeNotificationStatus(1));
        checkboxNewMessages.setOnClickListener(view -> changeNotificationStatus(2));
        checkboxSupportMessages.setOnClickListener(view -> changeNotificationStatus(3));
    }

    private void checkAppLang() {
        String currentLocale = getResources().getConfiguration().locale.getLanguage();
        if (currentLocale.equals("en"))
            appLanguage.setText("English");
        else if (currentLocale.equals("az"))
            appLanguage.setText("Azərbaycan dili");
        else if (currentLocale.equals("ru"))
            appLanguage.setText("Русский язык");
    }

    @OnClick({R.id.changeUserProfile, R.id.selectNewMatches, R.id.selectNewMessages, R.id.selectSupportMessages, R.id.selectLanguage, R.id.selectContacts, R.id.selectFAQ, R.id.selectFacebook, R.id.selectInstagram, R.id.selectLogout, R.id.selectBlackList,})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.changeUserProfile:
                startNewActivity(SettingsActivity.this, EditProfileActivity.class);
                break;
            case R.id.selectNewMatches:
                changeNotificationStatus(1);
                break;
            case R.id.selectNewMessages:
                changeNotificationStatus(2);
                break;
            case R.id.selectSupportMessages:
                changeNotificationStatus(3);
                break;
            case R.id.selectLanguage:
                startNewActivity(this, SelectLanguageActivity.class);
                finish();
                break;
            case R.id.selectBlackList:
                startNewActivity(this, BlackListActivity.class);
                break;
            case R.id.selectContacts:
                startNewActivity(this, ContactsActivity.class);
                break;
            case R.id.selectFAQ:
                startNewActivity(this, FAQActivity.class);
                break;
            case R.id.selectFacebook:
                openLink("https://www.facebook.com/derby.azerbaijan/");
                break;
            case R.id.selectInstagram:
                openLink("https://instagram.com/derby.az");
                break;
            case R.id.selectLogout:
                SessionManager.putString(SessionKeys.TOKEN.getKey(), "");
                LoginManager.getInstance().logOut();
                startActivityForResult(new Intent(this, SplashActivity.class), 0);
                finish();
                break;
        }
    }

    private void openLink(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void changeNotificationStatus(int type) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().notificationTypeChange(type, body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    getProfielInfo();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void getProfielInfo() {
        Call<Profile> call = APIUtils.getAPI().meProfile(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        userFullName.setText(response.body().getData().getFullname());
                        checkboxNewMatcthes.setChecked(response.body().getData().getSettings().getNewMatches());
                        checkboxNewMessages.setChecked(response.body().getData().getSettings().getNewMessages());
                        checkboxSupportMessages.setChecked(response.body().getData().getSettings().getSupportMessages());
                    }
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

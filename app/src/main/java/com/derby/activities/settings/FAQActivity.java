package com.derby.activities.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.activities.game.ChooseAddressActivity;
import com.derby.models.faq.Datum;
import com.derby.models.faq.FAQ;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.List;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-07-12 long live Chingiz!
 */
public class FAQActivity extends BaseActivity {

    @BindView(R.id.listView)
    ListView listView;

    private String[] faqNames;
    private List<Datum> faq;
    private String currentLang;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.f_a_q));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        currentLang = SessionManager.getString(SessionKeys.APP_LANG.getKey(), "");
        faqList();
    }

    private void faqList() {
        Call<FAQ> call = APIUtils.getAPI().faqList();
        call.enqueue(new Callback<FAQ>() {
            @Override
            public void onResponse(Call<FAQ> call, Response<FAQ> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        faq = response.body().getData();
                        faqNames = new String[response.body().getData().size()];
                        for (int i=0; i<response.body().getData().size(); i++) {
                            if (currentLang.equals("az"))
                                faqNames[i] = response.body().getData().get(i).getQuestion().getAz();
                            else if (currentLang.equals("en"))
                                faqNames[i] = response.body().getData().get(i).getQuestion().getEn();
                            else if (currentLang.equals("ru"))
                                faqNames[i] = response.body().getData().get(i).getQuestion().getRu();
                        }
                        ArrayAdapter<String> itemsAdapter =
                                new ArrayAdapter<>(FAQActivity.this, R.layout.list_faq, android.R.id.text1, faqNames);
                        listView.setAdapter(itemsAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<FAQ> call, Throwable t) {

            }
        });

        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            Intent intent = new Intent(FAQActivity.this, FAQMoreInfoActivity.class);
            if (currentLang.equals("az")) {
                intent.putExtra("question", faq.get(i).getQuestion().getAz());
                intent.putExtra("answer", faq.get(i).getAnswer().getAz());
            } else if (currentLang.equals("en")) {
                intent.putExtra("question", faq.get(i).getQuestion().getEn());
                intent.putExtra("answer", faq.get(i).getAnswer().getEn());
            } else if (currentLang.equals("ru")) {
                intent.putExtra("question", faq.get(i).getQuestion().getRu());
                intent.putExtra("answer", faq.get(i).getAnswer().getRu());
            }
            startActivity(intent);
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

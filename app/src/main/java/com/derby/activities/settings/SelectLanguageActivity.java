package com.derby.activities.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.activities.SplashActivity;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.HashMap;
import java.util.Locale;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-07-11 long live Chingiz!
 */
public class SelectLanguageActivity extends BaseActivity {

    @BindView(R.id.select_az_ic)
    ImageView selectAzIc;
    @BindView(R.id.select_en_ic)
    ImageView selectEnIc;
    @BindView(R.id.select_ru_ic)
    ImageView selectRuIc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.language));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        checkAppLang();
    }

    private void checkAppLang() {
        String currentLocale = getResources().getConfiguration().locale.getLanguage();
        if (currentLocale.equals("en"))
            selectEnIc.setVisibility(View.VISIBLE);
        else if (currentLocale.equals("az"))
            selectAzIc.setVisibility(View.VISIBLE);
        else if (currentLocale.equals("ru"))
            selectRuIc.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.select_az_ic, R.id.select_az, R.id.select_en_ic, R.id.select_en, R.id.select_ru_ic, R.id.select_ru})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.select_az_ic:
                selectLang("az");
                changeLang(2);
                break;
            case R.id.select_az:
                selectLang("az");
                changeLang(2);
                break;
            case R.id.select_en_ic:
                selectLang("en");
                changeLang(1);
                break;
            case R.id.select_en:
                selectLang("en");
                changeLang(1);
                break;
            case R.id.select_ru_ic:
                selectLang("ru");
                changeLang(3);
                break;
            case R.id.select_ru:
                selectLang("ru");
                changeLang(3);
                break;
        }
    }

    private void selectLang(String lang) {
        LocaleChanger.setLocale(new Locale(lang));
        SessionManager.putString(SessionKeys.APP_LANG.getKey(), lang);
        startActivityForResult(new Intent(this, SplashActivity.class), 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }

    private void changeLang(int id) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("id", id);
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().changeLanguage(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {

                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}

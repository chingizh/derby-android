package com.derby.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.derby.R;
import com.derby.adapters.StadiumImagesPagerAdapter;
import com.franmontiel.localechanger.LocaleChanger;
import com.rd.PageIndicatorView;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by chingizh on 2019-06-30 long live Chingiz!
 */
public class StadiumInfoActivity extends BaseActivity {

    @BindView(R.id.stadium_name)
    TextView stadiumName;
    @BindView(R.id.stadium_price)
    TextView stadiumPrice;
    @BindView(R.id.stadium_address)
    TextView stadiumAddress;
    @BindView(R.id.stadium_dimension)
    TextView stadiumDimension;
    @BindView(R.id.btn_call)
    Button btnCall;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    StadiumImagesPagerAdapter pagerAdapter;
    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;
    @BindView(R.id.imageView8)
    ImageView stadiumDimensionIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stadium_info);
        ButterKnife.bind(this);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle data = getIntent().getExtras();
        setTitle(data.getString("name"));
        stadiumName.setText(data.getString("name"));
        stadiumPrice.setText(data.getString("price"));
        stadiumAddress.setText(data.getString("address"));
        if (data.getString("dimensions").isEmpty()) {
            stadiumDimension.setVisibility(View.GONE);
            stadiumDimensionIcon.setVisibility(View.GONE);
        } else
            stadiumDimension.setText(data.getString("dimensions"));
        btnCall.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + data.getString("contact")));
            startActivity(intent);
        });

//        String[] images = data.getStringArray("images");
//        Log.d("", "onCreate: array length "+images.length);
        pagerAdapter = new StadiumImagesPagerAdapter(this, data.getStringArrayList("images"));
        viewPager.setAdapter(pagerAdapter);
        pageIndicatorView.setViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

package com.derby.activities.login;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.derby.R;
import com.derby.activities.BaseActivity;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-07-16 long live Chingiz!
 */
public class ForgetPasswordActivity extends BaseActivity {

    @BindView(R.id.input_email)
    TextInputEditText inputEmail;
    @BindView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.title_forget_password));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }

    @OnClick(R.id.sendEmail)
    public void onViewClicked() {
        sendEmail();
    }

    private void sendEmail() {
        Call<String> call = APIUtils.getAPI().forgetPassword1(inputEmail.getText().toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        Intent i = new Intent(ForgetPasswordActivity.this, ForgetPassword2Activity.class);
                        i.putExtra("email", inputEmail.getText().toString());
                        startActivity(i);
                        finish();
                    } else {
                        inputLayoutEmail.setError(getString(R.string.email_is_incorrect));
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}

package com.derby.activities.login;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.activities.MainActivity;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 5/6/19 long live Chingiz!
 */
public class SignUpActivity extends BaseActivity {

    static TextInputEditText inputBirthdate;

    @BindView(R.id.terms_conditions)
    TextView termsConditions;
    @BindView(R.id.sign_up)
    Button signUp;
    @BindView(R.id.gobackLogin)
    TextView gobackLogin;
    @BindView(R.id.input_fullname)
    TextInputEditText inputFullname;
    @BindView(R.id.input_layout_fullname)
    TextInputLayout inputLayoutFullname;
    @BindView(R.id.input_email)
    TextInputEditText inputEmail;
    @BindView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;
    @BindView(R.id.input_layout_birthdate)
    TextInputLayout inputLayoutBirthdate;
    @BindView(R.id.input_password)
    TextInputEditText inputPassword;
    @BindView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;
    @BindView(R.id.input_c_password)
    TextInputEditText inputCPassword;
    @BindView(R.id.input_layout_c_password)
    TextInputLayout inputLayoutCPassword;
    final Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setTitle(getString(R.string.sign_up));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        inputBirthdate = findViewById(R.id.input_birthdate);
        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };

        inputBirthdate.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            new DatePickerDialog(SignUpActivity.this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        inputBirthdate.setText(sdf.format(myCalendar.getTime()));
    }

    @OnClick({R.id.terms_conditions, R.id.sign_up, R.id.gobackLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.terms_conditions:
                break;
            case R.id.sign_up:
                if (validate()) {
                    signUp();
                }
                break;
            case R.id.gobackLogin:
                finish();
                break;
        }
    }

    private void signUp() {
        HashMap<String, Object> body = new HashMap<>();
        body.clear();
        inputLayoutFullname.setErrorEnabled(false);
        inputLayoutEmail.setErrorEnabled(false);
        inputLayoutBirthdate.setErrorEnabled(false);
        inputLayoutPassword.setErrorEnabled(false);
        inputLayoutCPassword.setErrorEnabled(false);
        body.put("fullname", inputFullname.getText().toString());
        body.put("email", inputEmail.getText().toString());
        body.put("birthdate", inputBirthdate.getText().toString());
        body.put("password", inputPassword.getText().toString());
        body.put("confirmPassword", inputCPassword.getText().toString());
        body.put("photoFileId", 0);
        Call<String> call = APIUtils.getAPI().register(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("sessionToken")) {
                        try {
                            JSONObject responseObject = new JSONObject(response.body());
                            JSONObject data = responseObject.getJSONObject("data");
                            SessionManager.putString(SessionKeys.TOKEN.getKey(), data.getString("sessionToken"));
                            SessionManager.putInt(SessionKeys.USER_ID.getKey(), data.getInt("id"));
                            SessionManager.putString(SessionKeys.USER_FULLNAME.getKey(), data.getString("fullname"));
                            SessionManager.putString(SessionKeys.USER_EMAIL.getKey(), data.getString("email"));
                            OneSignal.sendTag("user_id", String.valueOf(data.getInt("id")));
                            SessionManager.putBoolean(SessionKeys.FIRST_TIME.getKey(), true);
                            Intent i = new Intent(SignUpActivity.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        inputLayoutEmail.setError(getApplicationContext().getString(R.string.email_exists));
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private boolean validate() {
        inputLayoutFullname.setErrorEnabled(true);
        inputLayoutEmail.setErrorEnabled(true);
        inputLayoutBirthdate.setErrorEnabled(true);
        inputLayoutPassword.setErrorEnabled(true);
        inputLayoutCPassword.setErrorEnabled(true);
        if (inputFullname.getText().toString().isEmpty() ||
                inputEmail.getText().toString().isEmpty() ||
                inputBirthdate.getText().toString().isEmpty() ||
                inputPassword.getText().toString().isEmpty() ||
                inputCPassword.getText().toString().isEmpty()) {
            if (inputFullname.getText().toString().isEmpty())
                inputLayoutFullname.setError(getString(R.string.fill_in_blank));
            if (inputEmail.getText().toString().isEmpty())
                inputLayoutEmail.setError(getString(R.string.fill_in_blank));
            if (inputBirthdate.getText().toString().isEmpty())
                inputLayoutBirthdate.setError(getString(R.string.fill_in_blank));
            if (inputPassword.getText().toString().isEmpty())
                inputLayoutPassword.setError(getString(R.string.fill_in_blank));
            if (inputCPassword.getText().toString().isEmpty())
                inputLayoutCPassword.setError(getString(R.string.fill_in_blank));
            return false;
        } else if (!inputPassword.getText().toString().equals(inputCPassword.getText().toString())) {
            inputLayoutPassword.setError(getString(R.string.password_doesnt_match));
            inputLayoutCPassword.setError(getString(R.string.password_doesnt_match));
            return false;
        } else
            return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

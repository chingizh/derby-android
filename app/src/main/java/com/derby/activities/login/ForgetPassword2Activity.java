package com.derby.activities.login;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.derby.R;
import com.derby.activities.BaseActivity;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.HashMap;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-07-16 long live Chingiz!
 */
public class ForgetPassword2Activity extends BaseActivity {
    @BindView(R.id.input_email)
    TextInputEditText inputEmail;
    @BindView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;

    String email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password_2);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.title_forget_password));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        email = getIntent().getExtras().getString("email");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }

    private void sendCode() {
        HashMap<String, Object> body = new HashMap<>();
        body.put("code", inputEmail.getText().toString());
        body.put("email", email);
        Call<String> call = APIUtils.getAPI().forgetPassword2(body, inputEmail.getText().toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        Intent i = new Intent(ForgetPassword2Activity.this, ForgetPassword3Activity.class);
                        i.putExtra("email", email);
                        i.putExtra("code", inputEmail.getText().toString());
                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.sendEmail)
    public void onViewClicked() {
        sendCode();
    }
}

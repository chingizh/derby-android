package com.derby.activities.login;

import com.google.android.material.textfield.TextInputEditText;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.derby.R;
import com.derby.activities.BaseActivity;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import java.util.HashMap;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-07-16 long live Chingiz!
 */
public class ForgetPassword3Activity extends BaseActivity {
    @BindView(R.id.input_password)
    TextInputEditText inputPassword;
    @BindView(R.id.input_password_again)
    TextInputEditText inputPasswordAgain;
    String email, code;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password_3);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.title_forget_password));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        email = getIntent().getExtras().getString("email");
        code = getIntent().getExtras().getString("code");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }

    @OnClick(R.id.sendEmail)
    public void onViewClicked() {
        changePassword();
    }

    private void changePassword() {
        HashMap<String, Object> body = new HashMap<>();
        body.put("email", email);
        body.put("code", code);
        body.put("password", inputPassword.getText().toString());
        body.put("confirmPassword", inputPasswordAgain.getText().toString());
        Call<String> call = APIUtils.getAPI().forgetPassword3(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        finish();
                        Toast.makeText(ForgetPassword3Activity.this, getString(R.string.password_changed), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}

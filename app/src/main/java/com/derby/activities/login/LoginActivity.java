package com.derby.activities.login;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.activities.MainActivity;
import com.derby.rest.APIUtils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.franmontiel.localechanger.LocaleChanger;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 5/6/19 long live Chingiz!
 */
public class LoginActivity extends BaseActivity {

    @BindView(R.id.input_mail)
    TextInputEditText inputMail;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.btn_sign_in)
    Button btnSignIn;
    @BindView(R.id.forgot_password)
    TextView forgotPassword;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.login_button)
    LoginButton loginButton;
    @BindView(R.id.fb)
    ImageView fb;
    @BindView(R.id.signup_button)
    TextView signupButton;
    @BindView(R.id.input_layout_mail)
    TextInputLayout inputLayoutMail;
    @BindView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;

    CallbackManager callbackManager;

    private static final String EMAIL = "email";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
//                        Toast.makeText(LoginActivity.this, loginResult.getAccessToken().getToken()+"", Toast.LENGTH_SHORT).show();
                        registerFacebook(loginResult.getAccessToken().getToken());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(LoginActivity.this, getString(R.string.there_was_aproblem), Toast.LENGTH_SHORT).show();
                        Log.d("facebook problem", "onError: "+ exception.toString());
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick({R.id.btn_sign_in, R.id.btn_facebook, R.id.signup_button, R.id.forgot_password})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_in:
//                Intent i = new Intent(LoginActivity.this, MainActivity.class);
//                startActivity(i);
                if (validate()) {
                    normalLogin(inputMail.getText().toString(), inputPassword.getText().toString());
                }
                break;
            case R.id.signup_button:
                Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(i);
                break;
            case R.id.btn_facebook:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
                break;
            case R.id.forgot_password:
                startNewActivity(this, ForgetPasswordActivity.class);
        }
    }

    public void onClickFacebookButton() {
//        if (view == fb) {
//            loginButton.performClick();
//        }
    }

    private void normalLogin(String email, String password) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("email", email);
        body.put("password", password);
        Call<String> call = APIUtils.getAPI().login(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("sessionToken")) {
                        try {
                            JSONObject responseObject = new JSONObject(response.body());
                            JSONObject data = responseObject.getJSONObject("data");
                            SessionManager.putString(SessionKeys.TOKEN.getKey(), data.getString("sessionToken"));
                            SessionManager.putInt(SessionKeys.USER_ID.getKey(), data.getInt("id"));
                            SessionManager.putString(SessionKeys.USER_FULLNAME.getKey(), data.getString("fullname"));
                            SessionManager.putString(SessionKeys.USER_EMAIL.getKey(), data.getString("email"));
                            OneSignal.sendTag("user_id", String.valueOf(data.getInt("id")));
                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        inputLayoutMail.setError(getApplicationContext().getString(R.string.no_such_email));
                        inputLayoutPassword.setError(getApplicationContext().getString(R.string.password_wrong));
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("", "onFailure: " + t.toString());
            }
        });
    }

    private void registerFacebook(String accessToken) {
        Call<String> call = APIUtils.getAPI().registerFacebook(accessToken);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("sessionToken")) {
                        try {
                            JSONObject responseObject = new JSONObject(response.body());
                            JSONObject data = responseObject.getJSONObject("data");
                            SessionManager.putString(SessionKeys.TOKEN.getKey(), data.getString("sessionToken"));
                            SessionManager.putInt(SessionKeys.USER_ID.getKey(), data.getInt("id"));
                            SessionManager.putString(SessionKeys.USER_FULLNAME.getKey(), data.getString("fullname"));
                            SessionManager.putString(SessionKeys.USER_EMAIL.getKey(), data.getString("email"));
                            OneSignal.sendTag("user_id", String.valueOf(data.getInt("id")));
                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private boolean validate() {
        if (inputMail.getText().toString().isEmpty() && inputPassword.getText().toString().isEmpty()) {
            inputLayoutMail.setError(getString(R.string.fill_in_blank));
            inputLayoutPassword.setError(getString(R.string.fill_in_blank));
            return false;
        } else if (inputMail.getText().toString().isEmpty() || inputPassword.getText().toString().isEmpty()) {
            if (inputMail.getText().toString().isEmpty()) {
                inputLayoutMail.setError(getString(R.string.fill_in_blank));
            } else {
                inputLayoutPassword.setError(getString(R.string.fill_in_blank));
            }
            return false;
        } else
            return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

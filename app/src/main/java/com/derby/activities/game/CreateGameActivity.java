package com.derby.activities.game;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Toast;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.models.games.Datum;
import com.derby.models.games.Games;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-06-26 long live Chingiz!
 */
public class CreateGameActivity extends BaseActivity {

    @BindView(R.id.calendarView)
    CalendarView calendarView;
    @BindView(R.id.input_clock)
    TextInputEditText inputClock;
    @BindView(R.id.input_layout_clock)
    TextInputLayout inputLayoutClock;
    @BindView(R.id.input_address)
    TextInputEditText inputAddress;
    @BindView(R.id.input_layout_address)
    TextInputLayout inputLayoutAddress;
    @BindView(R.id.create_game)
    Button createGame;

    TimePickerDialog timePickerDialog;
    Calendar calendar;
    int currentHour;
    int currentMinute;

    String selectedDate = "";

    List<Datum> games;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_game);
        ButterKnife.bind(this);
        setTitle(getString(R.string.create_new_game));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        selectedDate = getDate(calendarView.getDate(), "dd/MM/yyyy");
        calendarView.setOnDateChangeListener((calendarView, year, month, dayOfMonth) ->
                selectedDate = (dayOfMonth + "/" + (month + 1) + "/" + year)
        );

        SessionManager.putString(SessionKeys.ADDRESS.getKey(), "");
        SessionManager.putInt(SessionKeys.ADDRESS_ID.getKey(), 0);

        allGames();
    }

    @OnClick({R.id.input_clock, R.id.input_address, R.id.create_game})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.input_clock:
                setTime();
                break;
            case R.id.input_address:
                startNewActivity(CreateGameActivity.this, ChooseAddressActivity.class);
                break;
            case R.id.create_game:
//                if (validate())
                createGame();
                break;
        }
    }

    private void setTime() {
        calendar = Calendar.getInstance();
        currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        currentMinute = calendar.get(Calendar.MINUTE);

        timePickerDialog = new TimePickerDialog(CreateGameActivity.this, (timePicker, hourOfDay, minutes) -> {
            inputClock.setText(String.format("%02d:%02d", hourOfDay, minutes));
        }, currentHour, currentMinute, false);

        timePickerDialog.show();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        inputAddress.setText(SessionManager.getString(SessionKeys.ADDRESS.getKey(), ""));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkIfTodayGameCreated() {
        ArrayList<String> todayGames = new ArrayList<>();
        for (int i = 0; i < games.size(); i++) {
            if (games.get(i).getStartDay().equals(selectedDate))
                todayGames.add(games.get(i).getStartDay());
        }
        return todayGames.size() == 0;
    }

    private void createGame() {
        HashMap<String, Object> body = new HashMap<>();

        body.put("addressId", SessionManager.getInt(SessionKeys.ADDRESS_ID.getKey(), 0));
        body.put("id", SessionManager.getInt(SessionKeys.USER_ID.getKey(), 0));
        body.put("startDay", selectedDate);
        body.put("startTime", inputClock.getText().toString());
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));

        if (checkIfTodayGameCreated()) {
            Call<String> call = APIUtils.getAPI().createGame(body);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        if (response.body().contains("OK")) {
                            startNewActivity(CreateGameActivity.this, GameCreatedActivity.class);
                            finish();
                        } else if (response.body().contains("ERROR") && response.body().contains("komanda")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body());
                                String currentLang = SessionManager.getString(SessionKeys.APP_LANG.getKey(), "");
                                JSONObject message = jsonObject.getJSONObject("message");
                                if (currentLang.equals("az"))
                                    Toast.makeText(CreateGameActivity.this, message.getString("az"), Toast.LENGTH_SHORT).show();
                                else if (currentLang.equals("en"))
                                    Toast.makeText(CreateGameActivity.this, message.getString("en"), Toast.LENGTH_SHORT).show();
                                else if (currentLang.equals("ru"))
                                    Toast.makeText(CreateGameActivity.this, message.getString("ru"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {

                }
            });
        } else {
            Toast.makeText(this, getString(R.string.you_created_game), Toast.LENGTH_LONG).show();
        }
    }

    private void allGames() {
        Call<Games> call = APIUtils.getAPI().gameList(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""), "", 1, 10);
        call.enqueue(new Callback<Games>() {
            @Override
            public void onResponse(Call<Games> call, Response<Games> response) {
                if (response.isSuccessful()) {
                    games = response.body().getData();
                }
            }

            @Override
            public void onFailure(Call<Games> call, Throwable t) {

            }
        });
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    private boolean validate() {
        if (inputClock.getText().toString().isEmpty() || inputAddress.getText().toString().isEmpty()) {
            if (inputClock.getText().toString().isEmpty()) {
                inputLayoutClock.setError(getApplicationContext().getString(R.string.please_choose_clock));
            }
            if (inputAddress.getText().toString().isEmpty()) {
                inputLayoutAddress.setError(getApplicationContext().getString(R.string.please_choose_address));
            }
            return false;
        } else
            return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

package com.derby.activities.game;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;

import com.derby.R;
import com.derby.Utils;
import com.derby.activities.BaseActivity;
import com.franmontiel.localechanger.LocaleChanger;

import androidx.annotation.Nullable;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by chingizh on 2019-06-26 long live Chingiz!
 */
public class GameCreatedActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_created);
        ButterKnife.bind(this);
        Utils.changeStatusBar(GameCreatedActivity.this, R.color.game_created_statusbar_color);
        MediaPlayer mPlayer = MediaPlayer.create(GameCreatedActivity.this, R.raw.whistle);
        mPlayer.start();

    }

    @OnClick(R.id.btn_close)
    public void onViewClicked() {
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

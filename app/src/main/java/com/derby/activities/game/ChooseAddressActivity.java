package com.derby.activities.game;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.derby.NonScrollListView;
import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.models.address_list.AddressList;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-06-26 long live Chingiz!
 */
public class ChooseAddressActivity extends BaseActivity {

    @BindView(R.id.stadium_list_view)
    NonScrollListView stadiumListView;
    @BindView(R.id.titleOther)
    TextView titleOther;
    @BindView(R.id.stadium_list_view_other)
    NonScrollListView stadiumListViewOther;
    String[] stadiumName;
    int[] stadiumId;
    String[] stadiumNameOther;
    int[] stadiumIdOther;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_address);
        ButterKnife.bind(this);
        setTitle(getString(R.string.address));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getAddressList();
//        ViewCompat.setNestedScrollingEnabled(stadiumListView, true);
//        ViewCompat.setNestedScrollingEnabled(stadiumListViewOther, true);
//        stadiumListView.setOnTouchListener((v, event) -> {
//            // Disallow the touch request for parent scroll on touch of child view
//            v.getParent().requestDisallowInterceptTouchEvent(true);
//            return false;
//        });
//        stadiumListViewOther.setOnTouchListener((v, event) -> {
//            // Disallow the touch request for parent scroll on touch of child view
//            v.getParent().requestDisallowInterceptTouchEvent(true);
//            return false;
//        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void getAddressList() {
        Call<AddressList> call = APIUtils.getAPI().addressList();
        call.enqueue(new Callback<AddressList>() {
            @Override
            public void onResponse(Call<AddressList> call, Response<AddressList> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        stadiumId = new int[response.body().getData().get(0).getCity().size()];
                        stadiumName = new String[response.body().getData().get(0).getCity().size()];
                        stadiumIdOther = new int[response.body().getData().get(1).getCity().size()];
                        stadiumNameOther = new String[response.body().getData().get(1).getCity().size()];
                        String currentlang = SessionManager.getString(SessionKeys.APP_LANG.getKey(), "");
                        for (int i = 0; i < response.body().getData().get(0).getCity().size(); i++) {
                            stadiumId[i] = response.body().getData().get(0).getCity().get(i).getId();
                            if (currentlang.equals("az"))
                                stadiumName[i] = response.body().getData().get(0).getCity().get(i).getValue().getAz();
                            else if (currentlang.equals("en"))
                                stadiumName[i] = response.body().getData().get(0).getCity().get(i).getValue().getEn();
                            else if (currentlang.equals("ru"))
                                stadiumName[i] = response.body().getData().get(0).getCity().get(i).getValue().getRu();
                        }
                        for (int i = 0; i < response.body().getData().get(1).getCity().size(); i++) {
                            stadiumIdOther[i] = response.body().getData().get(1).getCity().get(i).getId();
                            if (currentlang.equals("az"))
                                stadiumNameOther[i] = response.body().getData().get(1).getCity().get(i).getValue().getAz();
                            else if (currentlang.equals("en"))
                                stadiumNameOther[i] = response.body().getData().get(1).getCity().get(i).getValue().getEn();
                            else if (currentlang.equals("ru"))
                                stadiumNameOther[i] = response.body().getData().get(1).getCity().get(i).getValue().getRu();
                        }
                        ArrayAdapter<String> itemsAdapter =
                                new ArrayAdapter<>(ChooseAddressActivity.this, R.layout.list_address, android.R.id.text1, stadiumName);
                        stadiumListView.setAdapter(itemsAdapter);

                        ArrayAdapter<String> itemsAdapter1 =
                                new ArrayAdapter<>(ChooseAddressActivity.this, R.layout.list_address, android.R.id.text1, stadiumNameOther);
                        stadiumListViewOther.setAdapter(itemsAdapter1);

//                        setListViewHeightBasedOnChildren(stadiumListView);
//                        setListViewHeightBasedOnChildren(stadiumListViewOther);
                    }
                }
            }

            @Override
            public void onFailure(Call<AddressList> call, Throwable t) {

            }
        });

        stadiumListView.setOnItemClickListener((adapterView, view, i, l) -> {
            SessionManager.putString(SessionKeys.ADDRESS.getKey(), stadiumName[i]);
            SessionManager.putInt(SessionKeys.ADDRESS_ID.getKey(), stadiumId[i]);
            finish();
        });

        stadiumListViewOther.setOnItemClickListener((adapterView, view, i, l) -> {
            SessionManager.putString(SessionKeys.ADDRESS.getKey(), stadiumNameOther[i]);
            SessionManager.putInt(SessionKeys.ADDRESS_ID.getKey(), stadiumIdOther[i]);
            finish();
        });
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

package com.derby.activities.game;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.BaseActivity;
import com.derby.activities.messages.ChatActivity;
import com.derby.models.messages.Messages;
import com.derby.rest.APIUtils;
import com.franmontiel.localechanger.LocaleChanger;

import de.hdodenhof.circleimageview.CircleImageView;

import java.util.HashMap;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-06-30 long live Chingiz!
 */
public class GameDetailActivity extends BaseActivity {

    @BindView(R.id.team_logo)
    CircleImageView teamLogo;
    @BindView(R.id.textView3)
    TextView game_name;
    @BindView(R.id.game_time)
    TextView gameTime;
    @BindView(R.id.game_date)
    TextView gameDate;

    @BindView(R.id.btn_accept_time)
    Button btnAcceptTime;
    @BindView(R.id.game_address)
    TextView gameAddress;

    private int gameId;
    private int userId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_detail);
        ButterKnife.bind(this);
        setTitle(getApplicationContext().getString(R.string.game_detail));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle data = getIntent().getExtras();

        if (!data.getString("logo_url").isEmpty())
            Glide.with(getApplicationContext()).load(data.getString("logo_url")).into(teamLogo);
        game_name.setText(data.getString("name"));
        gameTime.setText(data.getString("start_time"));
        gameDate.setText(data.getString("start_day"));
        gameAddress.setText(data.getString("address"));
        gameId = data.getInt("id");
        userId = data.getInt("userId");
        btnAcceptTime.setOnClickListener(view -> acceptGame(gameId));
        if (data.getInt("createUserId") == SessionManager.getInt(SessionKeys.USER_ID.getKey(), 0)) {
            btnAcceptTime.setVisibility(View.GONE);
        }
    }

    private void acceptGame(int id) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().acceptGame(body, id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        getConverstationList();
                        finish();
                    } else {
                        Toast.makeText(GameDetailActivity.this, getString(R.string.this_game_already_accepted), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void getConverstationList() {
        Call<Messages> call = APIUtils.getAPI().conversationList(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<Messages>() {
            @Override
            public void onResponse(Call<Messages> call, Response<Messages> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                        intent.putExtra("title", response.body().getData().get(response.body().getData().size() - 1).getName());
                        intent.putExtra("type", response.body().getData().get(response.body().getData().size() - 1).getType().getValue().getAz());
                        intent.putExtra("userId", response.body().getData().get(response.body().getData().size() - 1).getId());
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<Messages> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_game_detail, menu);

        final MenuItem delete = menu.findItem(R.id.action_delete);
        if (userId == SessionManager.getInt(SessionKeys.USER_ID.getKey(), 0))
            delete.setVisible(true);
        else
            delete.setVisible(false);

        delete.setOnMenuItemClickListener(menuItem -> {
            removeGame(gameId);
            return false;
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void removeGame(int id) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().removeGame(body, id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }
}

package com.derby.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.messages.ChatActivity;
import com.derby.adapters.MessageRecyclerViewAdapter;
import com.derby.models.messages.Datum;
import com.derby.models.messages.Messages;
import com.derby.rest.APIUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;
import ua.naiksoftware.stomp.dto.StompHeader;

import static com.facebook.AccessTokenManager.TAG;

/**
 * Created by chingizh on 6/18/19 long live Chingiz!
 */
public class MessageFragment extends BaseFragment {

    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;
    private RecyclerView messagesRecyclerView;

    private MessageRecyclerViewAdapter mAdapter;

    private List<Datum> modelList = new ArrayList<>();

    private StompClient mStompClient;

    private CompositeDisposable compositeDisposable;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_message, container, false);
        ButterKnife.bind(this, v);
        messagesRecyclerView = v.findViewById(R.id.messages_recyclerview);

        startConnection();

        swipeToRefresh.setOnRefreshListener(this::getConverstationList);
        return v;
    }

    private void startConnection() {
        getConverstationList();
        mStompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, "http://134.209.231.73:8182/DerbyRest/chat");
        resetSubscriptions();
        connectStomp();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        startConnection();
    }

    @Override
    public void onResume() {
        super.onResume();
        startConnection();
    }

    private void getConverstationList() {
        Call<Messages> call = APIUtils.getAPI().conversationList(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<Messages>() {
            @Override
            public void onResponse(Call<Messages> call, Response<Messages> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        modelList = response.body().getData();
                        setAdapter(response.body().getData());
                        swipeToRefresh.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<Messages> call, Throwable t) {

            }
        });
    }

    private void setAdapter(List<Datum> modelList) {

        mAdapter = new MessageRecyclerViewAdapter(getActivity(), modelList);

        messagesRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        messagesRecyclerView.setLayoutManager(layoutManager);


        messagesRecyclerView.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener((view, position, model) -> {
            Intent intent = new Intent(getContext(), ChatActivity.class);
            intent.putExtra("title", model.getName());
            String currentlang = SessionManager.getString(SessionKeys.APP_LANG.getKey(), "");
            intent.putExtra("type", model.getType().getValue().getAz());
            intent.putExtra("conversID", model.getId());
            intent.putExtra("userId", model.getUserId());
            startActivity(intent);
        });

        mAdapter.SetOnItemLongClickListener((view, position, model) -> {
            if (SessionManager.getInt(SessionKeys.USER_ID.getKey(), 0) != model.getTeamCapitanId()) {
                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(getContext().getString(R.string.are_u_sure_to_hide_this_chat))
                        .setPositiveButton(getContext().getString(R.string.yes), (dialog, which) -> hideConversation(model.getId()))
                        .setNegativeButton(getContext().getString(R.string.no), null)
                        .show();
            }
        });
    }

    private void connectStomp() {

        List<StompHeader> headers = new ArrayList<>();
        headers.add(new StompHeader("Auth", SessionManager.getString(SessionKeys.TOKEN.getKey(), "")));

//        mStompClient.withClientHeartbeat(1000).withServerHeartbeat(1000);

        resetSubscriptions();

        Disposable dispLifecycle = mStompClient.lifecycle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(lifecycleEvent -> {
                    switch (lifecycleEvent.getType()) {
                        case OPENED:
                            System.out.println("Stomp connection opened");
                            break;
                        case ERROR:
                            Log.e(TAG, "Stomp connection error", lifecycleEvent.getException());
                            System.out.println("Stomp connection error");
                            break;
                        case CLOSED:
                            System.out.println("Stomp connection closed");
                            resetSubscriptions();
                            break;
                        case FAILED_SERVER_HEARTBEAT:
                            System.out.println("Stomp failed server heartbeat");
                            break;
                    }
                });

        compositeDisposable.add(dispLifecycle);

        String sessionToken = SessionManager.getString(SessionKeys.TOKEN.getKey(), "");

        // Receive greetings
        Disposable dispTopic = mStompClient.topic("/topic/messages/" + sessionToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(topicMessage -> {
                    Log.d(TAG, "Received " + topicMessage.getPayload());
                    getConverstationList();
                }, throwable -> {
                    Log.e(TAG, "Error on subscribe topic", throwable);
                });

        compositeDisposable.add(dispTopic);

        mStompClient.connect(headers);
    }

    private void disconnectStomp() {
        mStompClient.disconnect();
    }

    private void resetSubscriptions() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onDestroy() {
        disconnectStomp();
        if (compositeDisposable != null) compositeDisposable.dispose();
        super.onDestroy();
    }



    private void hideConversation(int id) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().hideConversation(body, id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        getConverstationList();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

}

package com.derby.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.messages.ChatActivity;
import com.derby.activities.team.AddTeamMemberActivityy;
import com.derby.adapters.RecyclerViewScrollListener;
import com.derby.adapters.TeamMembersAdapter;
import com.derby.models.AbstractModel;
import com.derby.models.team.Player;
import com.derby.models.team.Team;
import com.derby.rest.APIUtils;

import de.hdodenhof.circleimageview.CircleImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.AccessTokenManager.TAG;

/**
 * Created by chingizh on 5/11/19 long live Chingiz!
 */
public class TeamFragment extends BaseFragment {

    @BindView(R.id.add_name)
    EditText addName;
    @BindView(R.id.create_team)
    Button createTeam;
    @BindView(R.id.no_team)
    ConstraintLayout noTeam;
    @BindView(R.id.team_exist)
    ConstraintLayout teamExist;
    @BindView(R.id.imageView4)
    CircleImageView teamLogo;
    @BindView(R.id.team_photo)
    CircleImageView teamPhoto;
    @BindView(R.id.team_name)
    TextView teamName;
//    @BindView(R.id.team_members_size)
    TextView teamMembersSize;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;

    private TeamMembersAdapter mAdapter;
    private RecyclerViewScrollListener scrollListener;

    private ArrayList<AbstractModel> modelList = new ArrayList<>();

    View view;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_team, container, false);
        ButterKnife.bind(this, v);
        getTeam();
        recyclerView.setNestedScrollingEnabled(true);
        SessionManager.putBoolean(SessionKeys.FIRST_TIME.getKey(), false);
        swipeToRefresh.setOnRefreshListener(this::getTeam);
        teamMembersSize = v.findViewById(R.id.team_members_size);
        view = v;
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        getTeam();
    }

    @OnClick(R.id.create_team)
    public void onViewClicked() {
        createTeam();
    }

    private void createTeam() {
        HashMap<String, Object> body = new HashMap<>();
        body.put("name", addName.getText().toString());
        body.put("photoField", 0);
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().createTeam(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse: " + response.body());
                    noTeam.setVisibility(View.GONE);
                    teamExist.setVisibility(View.VISIBLE);
                    getTeam();
                    hideKeyboardFrom(getContext(), view);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void getTeam() {
        Call<Team> call = APIUtils.getAPI().meTeam(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<Team>() {
            @Override
            public void onResponse(Call<Team> call, Response<Team> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        if (teamMembersSize != null) {
                            noTeam.setVisibility(View.GONE);
                            teamExist.setVisibility(View.VISIBLE);
                            teamName.setText(response.body().getData().getName());
                            teamMembersSize.setText(String.format(getString(R.string.s_members), String.valueOf(response.body().getData().getPlayerCount())));
                            SessionManager.putInt(SessionKeys.TEAM_ID.getKey(), response.body().getData().getId());
                            if (response.body().getData().getLogoUrl().isEmpty())
                                Glide.with(getContext()).load(R.drawable.derby_teamlogotem_2).into(teamPhoto);
                            else
                                Glide.with(getContext()).load(response.body().getData().getLogoUrl()).into(teamPhoto);
                            setAdapter(response.body().getData().getPlayers());
                        }
//                        try {
//                            JSONObject responseObject = new JSONObject(response.body());
//                            JSONObject data = responseObject.getJSONObject("data");
//                            teamName.setText(data.getString("name"));
//                            teamMembersSize.setText(data.getJSONArray("players").length() + " members");
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        setAdapter();
                    } else {
                        noTeam.setVisibility(View.VISIBLE);
                        teamExist.setVisibility(View.GONE);
                    }
                    swipeToRefresh.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<Team> call, Throwable t) {

            }
        });
    }

    private void setAdapter(List<Player> modelList) {
        mAdapter = new TeamMembersAdapter(getActivity(), modelList, "Header");


        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_recyclerview));
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.setAdapter(mAdapter);


//        scrollListener = new RecyclerViewScrollListener() {
//
//            public void onEndOfScrollReached(RecyclerView rv) {
//
//                Toast.makeText(getActivity(), "End of the RecyclerView reached. Do your pagination stuff here", Toast.LENGTH_SHORT).show();
//
//                scrollListener.disableScrollListener();
//            }
//        };
//        recyclerView.addOnScrollListener(scrollListener);
//          /*
//             Note: The below two methods should be used wisely to handle the pagination enable and disable states based on the use case.
//                     1. scrollListener.disableScrollListener(); - Should be called to disable the scroll state.
//                     2. scrollListener.enableScrollListener(); - Should be called to enable the scroll state.
//          */


        mAdapter.SetOnItemClickListener((view, position, model) -> {

        });


        mAdapter.SetOnHeaderClickListener((view, headerTitle) -> {

            //handle item click events here
            Intent i = new Intent(getContext(), AddTeamMemberActivityy.class);
            startActivity(i);

        });

        mAdapter.SetOnItemLongClickListener((view, position, model) -> {
            if (position != 1)
                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage(getContext().getString(R.string.are_u_sure_to_delete_this_member))
                        .setPositiveButton(getContext().getString(R.string.yes), (dialog, which) -> removeTeamMember(model.getId()))
                        .setNegativeButton(getContext().getString(R.string.no), null)
                        .show();
            return false;
        });


        //TODO - Komandamda memberden mesaj kecid button yoxdu
        mAdapter.SetOnMessageItemClickListener((view, position, model) -> {
            if (!model.getTeamCapitan()) {
                Intent intent = new Intent(getContext(), ChatActivity.class);
                intent.putExtra("title", model.getUser().getFullname());
                intent.putExtra("userId", model.getUser().getId());
                intent.putExtra("type", "Private");
                intent.putExtra("conversID", model.getId());
                startActivity(intent);
            }
        });
    }

    private void removeTeamMember(int id) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("token", SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        Call<String> call = APIUtils.getAPI().removePlayerFromTeam(id, body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body().contains("OK")) {
                        getTeam();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}

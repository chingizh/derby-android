package com.derby.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.team.AcceptDeclineNotificationActivity;
import com.derby.adapters.NotificationsRecyclerViewAdapter;
import com.derby.models.notifications.Datum;
import com.derby.models.notifications.Notification;
import com.derby.rest.APIUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 5/16/19 long live Chingiz!
 */
public class NotificationFragment extends BaseFragment {

    @BindView(R.id.notification_recyclerview)
    RecyclerView notificationRecyclerview;
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, v);
        getNotification();
        notificationRecyclerview.setNestedScrollingEnabled(true);
        swipeToRefresh.setOnRefreshListener(this::getNotification);
        return v;
    }

    private void getNotification() {
        Call<Notification> call = APIUtils.getAPI().notificationList(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""), "", 1, 100);
        call.enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        setAdapter(response.body().getData());
                        swipeToRefresh.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<Notification> call, Throwable t) {

            }
        });
    }

    private void setAdapter(List<Datum> modelList) {

        NotificationsRecyclerViewAdapter mAdapter = new NotificationsRecyclerViewAdapter(getActivity(), modelList);

        notificationRecyclerview.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        notificationRecyclerview.setLayoutManager(layoutManager);


        notificationRecyclerview.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener((view, position, model) -> {

            //handle item click events here
//            Toast.makeText(getActivity(), "Hey " + model.getTitle(), Toast.LENGTH_SHORT).show();

        });
    }

    @OnClick({R.id.team_requests, R.id.ic_team_request, R.id.txt_team_requests, R.id.accept_or_decline})
    public void onViewClicked() {
        Intent i = new Intent(getContext(), AcceptDeclineNotificationActivity.class);
        startActivity(i);
    }
}

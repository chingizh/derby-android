package com.derby.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.game.GameDetailActivity;
import com.derby.adapters.AllGamesRecyclerViewAdapter;
import com.derby.adapters.RecyclerViewScrollListener;
import com.derby.adapters.UpcomingGamesRecyclerViewAdapter;
import com.derby.models.games.Datum;
import com.derby.models.games.Games;
import com.derby.rest.APIUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 5/11/19 long live Chingiz!
 */
public class GamesFragment extends BaseFragment {

    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;
    @BindView(R.id.no_game)
    TextView noGame;
    @BindView(R.id.content)
    ConstraintLayout content;
    @BindView(R.id.upcoming_games_tt)
    TextView upcomingGamesTt;
    @BindView(R.id.all_games_tt)
    TextView allGamesTt;
    //    @BindView(R.id.recycler_view_upcoming_games)
    private RecyclerView recyclerViewUpcomingGames;
    private RecyclerView recyclerViewAllGames;

    private RecyclerViewScrollListener scrollListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_games, container, false);
        ButterKnife.bind(this, v);
        recyclerViewUpcomingGames = v.findViewById(R.id.recycler_view_upcoming_games);
        recyclerViewAllGames = v.findViewById(R.id.recycler_view_all_games);
        recyclerViewAllGames.setNestedScrollingEnabled(true);
        recyclerViewUpcomingGames.setNestedScrollingEnabled(true);

        swipeToRefresh.setOnRefreshListener(this::refreshGames);
        return v;
    }

    private void noGame(boolean isGame) {
        if (isGame) {
            noGame.setVisibility(View.VISIBLE);
            upcomingGamesTt.setVisibility(View.GONE);
            recyclerViewUpcomingGames.setVisibility(View.GONE);
            allGamesTt.setVisibility(View.GONE);
            recyclerViewAllGames.setVisibility(View.GONE);
        } else {
            noGame.setVisibility(View.GONE);
            upcomingGamesTt.setVisibility(View.VISIBLE);
            recyclerViewUpcomingGames.setVisibility(View.VISIBLE);
            allGamesTt.setVisibility(View.VISIBLE);
            recyclerViewAllGames.setVisibility(View.VISIBLE);
        }
    }

    private void refreshGames() {
        allGames();
        swipeToRefresh.setRefreshing(false);
    }

    @Override
    public void onAttachFragment(@NonNull Fragment childFragment) {
        super.onAttachFragment(childFragment);
        allGames();
    }

    private void setAdapterAllGames(List<Datum> modelList) {
        AllGamesRecyclerViewAdapter allGamesRecyclerViewAdapter = new AllGamesRecyclerViewAdapter(getActivity(), modelList);

        recyclerViewAllGames.setHasFixedSize(true);


//        GridAutofitLayoutManager layoutManager = new GridAutofitLayoutManager(getContext(), 375);
        final GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerViewAllGames.setLayoutManager(layoutManager);


        recyclerViewAllGames.setAdapter(allGamesRecyclerViewAdapter);


        scrollListener = new RecyclerViewScrollListener() {

            public void onEndOfScrollReached(RecyclerView rv) {

//                Toast.makeText(getActivity(), "End of the RecyclerView reached. Do your pagination stuff here", Toast.LENGTH_SHORT).show();

                scrollListener.disableScrollListener();
            }
        };
        recyclerViewAllGames.addOnScrollListener(scrollListener);
          /*
             Note: The below two methods should be used wisely to handle the pagination enable and disable states based on the use case.
                     1. scrollListener.disableScrollListener(); - Should be called to disable the scroll state.
                     2. scrollListener.enableScrollListener(); - Should be called to enable the scroll state.
          */


        allGamesRecyclerViewAdapter.SetOnItemClickListener((view, position, model) -> {
            Intent i = new Intent(getContext(), GameDetailActivity.class);
            i.putExtra("logo_url", model.getCreateTeam().getLogoUrl());
            i.putExtra("name", model.getCreateTeam().getName());
            i.putExtra("start_time", model.getStartTime());
            i.putExtra("start_day", model.getStartDay());
            String currentlang = SessionManager.getString(SessionKeys.APP_LANG.getKey(), "");
            if (currentlang.equals("az"))
                i.putExtra("address", model.getAddress().getValue().getAz());
            else if (currentlang.equals("en"))
                i.putExtra("address", model.getAddress().getValue().getEn());
            else if (currentlang.equals("ru"))
                i.putExtra("address", model.getAddress().getValue().getRu());
            i.putExtra("id", model.getId());
            i.putExtra("createUserId", model.getCreateUserId());
            startActivity(i);
        });
    }

    private void setAdapterUpcomingGames(List<Datum> abstractModels) {
        UpcomingGamesRecyclerViewAdapter upcomingGamesRecyclerViewAdapter = new UpcomingGamesRecyclerViewAdapter(getActivity(), abstractModels);

        recyclerViewUpcomingGames.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewUpcomingGames.setLayoutManager(layoutManager);


        recyclerViewUpcomingGames.setAdapter(upcomingGamesRecyclerViewAdapter);


        scrollListener = new RecyclerViewScrollListener() {
            public void onEndOfScrollReached(RecyclerView rv) {

//                Toast.makeText(getActivity(), "End of the RecyclerView reached. Do your pagination stuff here", Toast.LENGTH_SHORT).show();

                scrollListener.disableScrollListener();
            }
        };
        recyclerViewUpcomingGames.addOnScrollListener(scrollListener);
          /*
             Note: The below two methods should be used wisely to handle the pagination enable and disable states based on the use case.
                     1. scrollListener.disableScrollListener(); - Should be called to disable the scroll state.
                     2. scrollListener.enableScrollListener(); - Should be called to enable the scroll state.
          */


        upcomingGamesRecyclerViewAdapter.SetOnItemClickListener((view, position, model) -> {
            Intent i = new Intent(getContext(), GameDetailActivity.class);
            i.putExtra("logo_url", model.getCreateTeam().getLogoUrl());
            i.putExtra("name", model.getCreateTeam().getName());
            i.putExtra("start_time", model.getStartTime());
            i.putExtra("start_day", model.getStartDay());
            String currentlang = SessionManager.getString(SessionKeys.APP_LANG.getKey(), "");
            if (currentlang.equals("az"))
                i.putExtra("address", model.getAddress().getValue().getAz());
            else if (currentlang.equals("en"))
                i.putExtra("address", model.getAddress().getValue().getEn());
            else if (currentlang.equals("ru"))
                i.putExtra("address", model.getAddress().getValue().getRu());
            i.putExtra("id", model.getId());
            i.putExtra("createUserId", model.getCreateUserId());
            startActivity(i);
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        allGames();
    }

    private void allGames() {
        Call<Games> call = APIUtils.getAPI().gameList(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""), "", 1, 10);
        call.enqueue(new Callback<Games>() {
            @Override
            public void onResponse(Call<Games> call, Response<Games> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData().size() == 0) {
                        noGame(true);
                    } else {
                        noGame(false);
                    }
                    setAdapterAllGames(response.body().getData());
                    setAdapterUpcomingGames(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<Games> call, Throwable t) {

            }
        });
    }
}

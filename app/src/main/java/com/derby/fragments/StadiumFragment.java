package com.derby.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.derby.R;
import com.derby.SessionKeys;
import com.derby.activities.StadiumInfoActivity;
import com.derby.adapters.StadiumViewAdapter;
import com.derby.models.stadium.Datum;
import com.derby.models.stadium.Stadium;
import com.derby.rest.APIUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import hundredthirtythree.sessionmanager.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chingizh on 2019-06-30 long live Chingiz!
 */
public class StadiumFragment extends BaseFragment {

    RecyclerView recyclerView;
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;

    private StadiumViewAdapter mAdapter;

//    private ArrayList<Datum> modelList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_stadium, container, false);
        ButterKnife.bind(this, v);
        recyclerView = v.findViewById(R.id.recycler_view);
        getStadiums();
        swipeToRefresh.setOnRefreshListener(this::getStadiums);
        return v;
    }

    private void getStadiums() {
        Call<Stadium> call = APIUtils.getAPI().stadiumList(SessionManager.getString(SessionKeys.TOKEN.getKey(), ""));
        call.enqueue(new Callback<Stadium>() {
            @Override
            public void onResponse(Call<Stadium> call, Response<Stadium> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode().equals("OK")) {
                        setAdapter(response.body().getData());
                        swipeToRefresh.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<Stadium> call, Throwable t) {

            }
        });
    }

    private void setAdapter(List<Datum> modelList) {
        mAdapter = new StadiumViewAdapter(getActivity(), modelList);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener((view, position, model) -> {
            Intent i = new Intent(getContext(), StadiumInfoActivity.class);
            i.putStringArrayListExtra("images", (ArrayList<String>) model.getImageUrl());
            i.putExtra("name", model.getName());
            i.putExtra("price", model.getPrice());
            i.putExtra("address", model.getAddress());
            i.putExtra("dimensions", model.getDimensions());
            i.putExtra("contact", model.getContact());
            startActivity(i);
        });
//        mAdapter.SetOnItemClickListener((view, position, model) -> Toast.makeText(getActivity(), "Hey " + model.getName(), Toast.LENGTH_SHORT).show());

    }
}

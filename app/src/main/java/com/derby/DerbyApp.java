package com.derby;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;

import com.derby.activities.SplashActivity;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.franmontiel.localechanger.LocaleChanger;
import com.onesignal.OneSignal;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import hundredthirtythree.sessionmanager.SessionManager;

/**
 * Created by chingizh on 6/9/19 long live Chingiz!
 */
public class DerbyApp extends Application {

    public static final List<Locale> SUPPORTED_LOCALES =
            Arrays.asList(
                    new Locale("en"),
                    new Locale("ru"),
                    new Locale("az")
            );

    @Override
    public void onCreate() {
        super.onCreate();
        AppEventsLogger.activateApp(this);
        new SessionManager.Builder()
                .setContext(getApplicationContext())
                .setPrefsName(SessionKeys.PREFS_NAME.getKey())
                .build();

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        LocaleChanger.initialize(getApplicationContext(), SUPPORTED_LOCALES);
        selectLang("az");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleChanger.onConfigurationChanged();
    }

    private void selectLang(String lang) {
        if (SessionManager.getString(SessionKeys.APP_LANG.getKey(), "").equals("")) {
            LocaleChanger.setLocale(new Locale(lang));
            SessionManager.putString(SessionKeys.APP_LANG.getKey(), lang);
        }
    }
}

package com.derby.rest;

import com.derby.models.add_team_member.AddTeamMember;
import com.derby.models.address_list.AddressList;
import com.derby.models.blacklist.BlackList;
import com.derby.models.converstationMessages.ConverstationMessages;
import com.derby.models.faq.FAQ;
import com.derby.models.games.Games;
import com.derby.models.messages.Messages;
import com.derby.models.profile.Profile;
import com.derby.models.stadium.Stadium;
import com.derby.models.team.Team;
import com.derby.models.notifications.Notification;
import com.derby.models.teamrequest.TeamRequest;

import java.io.File;
import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by chingizh on 6/9/19 long live Chingiz!
 */
public interface APIInterface {

    //Auth
    @POST("api/login")
    Call<String> login(@Body HashMap<String, Object> body);

    @POST("api/register")
    Call<String> register(@Body HashMap<String, Object> body);

    @POST("api/register/facebook/{accessToken}")
    Call<String> registerFacebook(@Path("accessToken") String accessToken);

    @POST("api/{email}/send")
    Call<String> forgetPassword1(@Path("email") String email);

    @POST("api/check/{code}")
    Call<String> forgetPassword2(@Body HashMap<String, Object> body, @Path("code") String code);

    @POST("api/change/password")
    Call<String> forgetPassword3(@Body HashMap<String, Object> body);


    //Conversation
    @GET("conversation/list")
    Call<Messages> conversationList(@Query("token") String token);

    @GET("conversation/user/{id}/messages")
    Call<ConverstationMessages> conversationBetweenUserList(@Path("id") int id, @Query("token") String token);
//                                                   @Query("keyword") String keyword,
//                                                   @Query("page") int page,
//                                                   @Query("pageSize") int pageSize);

    @POST("conversation/user/{id}/write/message")
    Call<String> sendMessageToUser(@Body HashMap<String, Object> body, @Path("id") int id);

    @GET("conversation/{id}/messages")
    Call<ConverstationMessages> converstationBetweenGroupList(@Path("id") int id, @Query("token") String token);
//                                                              @Query("keyword") String keyword,
//                                                              @Query("page") int page,
//                                                              @Query("pageSize") int pageSize);

    @POST("conversation/{id}/write/message")
    Call<String> sendMessageToGroup(@Body HashMap<String, Object> body, @Path("id") int id);

    @POST("conversation/{id}/hide")
    Call<String> hideConversation(@Body HashMap<String, Object> body, @Path("id") int id);


    //Game
    @POST("game/create")
    Call<String> createGame(@Body HashMap<String, Object> body);

    @GET("game/list")
    Call<Games> gameList(@Query("token") String token,
                         @Query("keyword") String keyword,
                         @Query("page") int page,
                         @Query("pageSize") int pageSize);

    @POST("game/{id}/accept")
    Call<String> acceptGame(@Body HashMap<String, Object> body, @Path("id") int id);

    @POST("game/{id}/remove")
    Call<String> removeGame(@Body HashMap<String, Object> body, @Path("id") int id);


    //Notification
    @GET("notification/list")
    Call<Notification> notificationList(@Query("token") String token,
                                        @Query("keyword") String keyword,
                                        @Query("page") int page,
                                        @Query("pageSize") int pageSize);

    @GET("notification/unread")
    Call<String> notificationUnread(@Query("token") String token);


    //Stadium
    @GET("stadium/list")
    Call<Stadium> stadiumList(@Query("token") String token);

    @GET("stadium/list")
    Call<String> stadiumListString(@Query("token") String token);

    @GET("stadium/{id}")
    Call<String> stadiumInfo(@Path("id") int id, @Query("token") String token);


    //Main
    @GET("main/me/profile")
    Call<Profile> meProfile(@Query("token") String token);

    @POST("main/me/profile/change")
    Call<String> changeProfile(@Body HashMap<String, Object> body);

    @POST("main/setting/notification/{type}/change")
    Call<String> notificationTypeChange(@Path("type") int type, @Body HashMap<String, Object> body);

    @POST("main/address/list")
    Call<AddressList> addressList();

    @GET("main/faq/list")
    Call<FAQ> faqList();

    @GET("main/blacklist")
    Call<BlackList> blackList(@Query("token") String token);

    @POST("main/blacklist/user/{id}/add")
    Call<String> addUserToBlackList(@Path("id") int id, @Body HashMap<String, Object> body);

    @POST("main/blacklist/{id}/remove")
    Call<String> removeUserFromBlackList(@Path("id") int id, @Body HashMap<String, Object> body);

    @Headers({
            "Content-Type: multipart/form-data",
            "Accept: application/json"
    })
    @Multipart
    @POST("main/file/upload")
    Call<String> uploadFile(@Part MultipartBody.Part file, @Part("token") String token);

    @POST("main/me/lang/change")
    Call<String> changeLanguage(@Body HashMap<String, Object> body);

    //Profile

    //Team
    @POST("team/create")
    Call<String> createTeam(@Body HashMap<String, Object> body);

    @POST("team/delete")
    Call<String> deleteTeam(@Body HashMap<String, Object> body);

    @GET("team/me")
    Call<Team> meTeam(@Query("token") String token);

    @POST("team/player/{id}/remove")
    Call<String> removePlayerFromTeam(@Path("id") int id, @Body HashMap<String, Object> body);

    @POST("team/request/{id}/accept")
    Call<String> acceptTeam(@Path("id") int id, @Body HashMap<String, Object> body);

    @POST("team/request/{id}/decline")
    Call<String> declineTeam(@Path("id") int id, @Body HashMap<String, Object> body);

    @GET("team/other/user/list")
    Call<AddTeamMember> otherUserList(@Query("token") String token);

    @POST("team/user/requests")
    Call<String> addMember(@Body HashMap<String, Object> body);

    @GET("team/request/list")
    Call<TeamRequest> teamRequestList(@Query("token") String token);

    @POST("team/update")
    Call<String> updateTeam(@Body HashMap<String, Object> body);
}

package com.derby.rest;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by chingizh on 6/9/19 long live Chingiz!
 */
public class APIClient {
    private static OkHttpClient client;

    //    private static String baseUrl = "http://derby.az:8182/DerbyRest/";
    private static String baseUrl = "http://derby.az/DerbyRest/";

    public static Retrofit getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        client = new OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .connectTimeout(100000, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();


        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }
}
